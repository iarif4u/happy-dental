<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChiefComplain extends Model
{
    protected $table = 'chief_complains';

    protected $fillable = ['complain_name'];
}
