<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExportImport extends Model
{
    protected $table = 'export_imports';
    protected $fillable =['medicine_name','contains','dosage_form','manufacturer','drugs_for'];
}
