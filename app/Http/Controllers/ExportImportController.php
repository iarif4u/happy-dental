<?php

namespace App\Http\Controllers;

use App\ExportImport;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;

class ExportImportController extends Controller
{
    public function getImportExportView(){
        return view('import_export.import_export');
    }

    /*Upload excel,csv etc file for medicine list*/
    public function uploadExcelFile(Request $request){

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = $file->getRealPath();

            $data = \Excel::load($path, function($reader) {

            })->get();

            if(!empty($data) && $data->count()){

                foreach ($data as $key => $value) {
                    if (!empty($value->medicine_name)){
                        $insert[] = [
                            'medicine_name' => $value->medicine_name,
                            'contains' => $value->contains,
                            'dosage_form' => $value->dosage_form,
                            'manufacturer' => $value->manufacturer,
                            'drugs_for' => $value->drugs_for,
                            'drugs_for' => $value->drugs_for,
                            'drugs_for' => $value->drugs_for,
                        ];

                    }

                }
                $fill_data = array_chunk($insert, 100);

                for ($i=0;$i<sizeof($fill_data);$i++){
                    DB::table('export_imports')->insert($fill_data[$i]);
                }
                echo '<div class="alert alert-success">Import successfully done</div>';
                die();
            }
        }else{
            echo '<div class="alert alert-success">File Does not found</div>';
        }
    }

    public function downloadExcel($type)
    {
        $data = ExportImport::select('medicine_name', 'contains', 'dosage_form', 'manufacturer','drugs_for')->get()->toArray();
        \Excel::create('medicine_'.date('d-m-Y'), function($excel) use ($data) {
            $excel->setTitle('Our new awesome title');
            $excel->sheet('medicine-list', function($sheet) use ($data){
                $sheet->fromArray($data);
            });
        })->download($type);
        return Redirect::back()->with('message', 'File export successfully done');
    }
}
