<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PatientController extends Controller
{

    /*Get view of add patient*/
    public function GetAddPatient(){
        return view('patient.addPatient');
    }

    /*post request by add pation form to inset patient data*/
    public function PostAddPatient(Request $request){
        $this->ValidationPatientAddReq($request);
        Patient::create([
            'name'=>$request->input('name'),
            'address'=>$request->input('address'),
            'age'=>$request->input('age'),
            'sex'=>$request->input('sex'),
            'mobile'=>$request->input('mobile'),
        ]);
        return Redirect::back()->with('message', 'The patient added success');

    }

    /*validate add patient post request*/
    /*param 1: Direct post request from form data*/
    private function ValidationPatientAddReq($request){
        $this->validate($request,[
            'name'=>'required|min:1|regex:/^[A-Za-z. -]+$/',
            'mobile'=>'required|min:11|max:11',
            'age'=>'required|min:1',
            'sex'=>'required|alpha|min:1',
            'address'=>'required|min:5',

        ],[
            'name.required'=>'Name is required',
            'name.regex'=>'Name is invalid',
            'mobile.required'=>'Mobile number is required',
            'mobile.min'=>'Mobile number is invalid',
            'mobile.max'=>'Mobile number is invalid',
            'age.required'=>'Patient age is required',
            'sex.required'=>'Patient gender is required',
            'address.required'=>'Address is required',
            'address.min'=>'Address is invalid',
        ]);
    }

    /*Get patient list by get req*/
    public function GetPatientList(){
        //reverse for id desc
        $patientList = Patient::all()->reverse();

        return view('patient.listPatient',['patientList'=>$patientList]);
    }

    public function UpdatePatient(Request $request){
        $validator = \Validator::make($request->all(), [
            'patientId' => 'required|min:1',
            'name' => 'required|string|min:1',
            'mobile' => 'required|numeric',
            'age' => 'required|numeric|min:1',
            'sex' => 'required',
            'address' => 'required|min:1',
        ], [
            'patientId.required' => 'Invalid Update Request',
            'patientId.min' => 'Invalid Update Request',
            'name.required' => 'Patient Name is Required',
            'mobile.required' => 'Mobile Number is Required',

            'mobile.numeric' => 'Mobile number must be contain number',
            'age.required' => 'Age is Required',
            'age.numeric' => 'Age is invalid, must be number',
            'address.required' => 'Address is required',
        ]);

        if ($validator->fails()) {
            echo "<div class='alert alert-danger text-center'>";
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                foreach ($messages as $mes) {
                    echo $mes . "<br/>";
                }
            }
            echo "</div>";
            die();
        }else{
            Patient::where(['id'=>$request->input('patientId')])->update([
                'name'=>$request->input('name'),
                'address'=>$request->input('address'),
                'age'=>$request->input('age'),
                'sex'=>$request->input('sex'),
                'mobile'=>$request->input('mobile'),
            ]);
        }
    }

    #patient delete
    public function DeletePatient(Request $request){
        $validator = \Validator::make($request->all(), [
            'patientId' => 'required|numeric|min:1',

        ], [
            'patientId.required' => 'Invalid Delete Request',
            'patientId.min' => 'Invalid Delete Request',
            'patientId.numeric' => 'Invalid Delete Request',

        ]);

        if ($validator->fails()) {
            echo "<div class='alert alert-danger text-center'>";
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                foreach ($messages as $mes) {
                    echo $mes . "<br/>";
                }
            }
            echo "</div>";
            die();
        }else{
            Patient::where(['id'=>$request->input('patientId')])->delete();
        }
    }
}

