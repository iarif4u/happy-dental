<?php

namespace App\Http\Controllers;

use App\Report;
use App\ReportPayment;
use App\ReportTreatmentCost;
use App\TreatmentCost;
use App\WorkSchedule;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /*Add payment schedule for report by report id*/
    public function GetReportPayment($reportId){
        $reportDetails = Report::with(['patient'])->where(['id'=>$reportId])->first();
        $previousTreatmentList = ReportTreatmentCost::where(['report_id'=>$reportId])->get();
        $paidList = ReportPayment::where(['report_id'=>$reportId])->get();
        $costList = TreatmentCost::all();
        $scheduleList = WorkSchedule::where(['report_id'=>$reportId])->get();
        if (!$reportDetails){
            return redirect()->route('home')->with('error',"Report doesn't found");
        }
        return view('payment.paymentReport',[
            'reportDetails'=>$reportDetails,
            'costList'=>$costList,
            'previousTreatmentList'=>$previousTreatmentList,
            'scheduleList'=>$scheduleList,
            'paidList'=>$paidList
        ]);
    }

    /*Payment Add form submit to this*/
    public function PostReportPayment($reportId,Request $request){
        $costList = TreatmentCost::all();
        $this->validate($request,[
            'total'=>'required',
            'treatment_name'=>'required|array',
            'rate'=>'required|array',
            'unit'=>'required|array',
        ],[
            'total.required'=>'Total amount is required',
            'treatment_name.required'=>'Treatment list is required',
            'treatment_name.array'=>'Treatment list is invalid',
            'rate.required'=>'Treatment cost list is required',
            'rate.array'=>'Treatment cost list is invalid',
            'unit.required'=>'Unit list is required',
            'unit.array'=>'Unit list is invalid',

        ]);
        $treatment_name = $request->input('treatment_name');
        if($treatment_name[0]==0){
            return redirect()->back()->with('error','Treatment name is required');
        }
        $unit = $request->input('unit');
        for($i =0; $i<sizeof($treatment_name); $i++){
            ReportTreatmentCost::create([
                'report_id'=>$reportId,
                'treatment_id'=>$treatment_name[$i],
                'treatment_cost_this_time'=>$costList->where('id',$treatment_name[$i])->first()->treatment_cost,
                'unit'=>$unit[$i],
                'date'=>date('d-m-Y'),
            ]);
        }
        return redirect()->back()->with('status','Report treatment cost has been added');
    }
    public function PaidClintFee(Request $request){
        $reportId =  $request->input('reportId');
        $amount =  $request->input('amount');
        $due =  $request->input('due');
        $validator = \Validator::make($request->all(),[
            'reportId'=>'required|min:1',
            'amount'=>'required|numeric',
        ],[
            'reportId.required'=>'Report number not found',
            'reportId.min'=>'Report number is invalid',
            'amount.required'=>'Amount not found',
            'amount.numeric'=>'Amount is invalid',
        ]);
        if($due>=$amount){
            if ($validator->fails()) {
                foreach ($validator->messages()->getMessages() as $field_name => $messages)
                {
                    foreach ($messages as $mes){
                        echo $mes."<br/>";
                    }
                }
                die();
            }else{
                ReportPayment::create([
                    'amount'=> $amount,
                    'date'=> date('d-m-Y'),
                    'time'=> date("h:i:s A"),
                    'report_id'=> $reportId,
                ]);
                echo null;
            }
        }else{
            echo "Paid amount is greater then due amount";
        }
    }
}
