<?php

namespace App\Http\Controllers;

use App\ExportImport;
use App\Medicine;
use App\Prescription;
use App\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use PDF;

class PrescriptionController extends Controller
{
    #Make prescription get view
    public function GetPrescriptionView($reportId){
        $reportDetails = Report::with('patient')->where(['id'=>$reportId])->first();
        return view('prescription.prescriptionView',['reportDetails'=>$reportDetails]);
    }

    #prescription make by post request
    public function PostMakePrescription($reportId,Request $request){

        $this->validate($request,[
            'c/c'=>'required',
            'o/e'=>'required',
            'advice'=>'required',
            'medicine_name'=>'required|array',
            'meal'=>'required|array',
            'morning'=>'required|array',
            'evening'=>'required|array',
            'night'=>'required|array',
        ],[
            'c/c.required'=>'C/C is required',
            'o/e.required'=>'Q/E is required',
            'advice.required'=>'Advice is required',
            'medicine_name.required'=>'Medicine name does not found',
            'medicine_name.array'=>'Medicine name is invalid',
            'meal.required'=>'Meal time is required',
            'meal.array'=>'Meal time is invalid',
            'morning.required'=>'Morning time is required',
            'morning.array'=>'Morning time is invalid',
            'evening.required'=>'Evening time is required',
            'evening.array'=>'Evening time is invalid',
            'night.required'=>'Night time is required',
            'night.array'=>'Night time is invalid',
        ]);

        $c_c = $request->input('c/c');
        $o_e = $request->input('o/e');
        $advice = $request->input('advice');
        $medicineName = $request->input('medicine_name');
        $meal = $request->input('meal');
        $morning = $request->input('morning');
        $evening = $request->input('evening');
        $night = $request->input('night');

        $prescription=    Prescription::create([
                                'report_id'=>$reportId,
                                'date'=>date('d-m-Y'),
                                'time'=>date('h:i A'),
                                'c_c'=>$c_c,
                                'o_e'=>$o_e,
                                'advice'=>$advice
                            ]);
        $prescriptionId = $prescription->id;
        for ($i =0; $i<sizeof($medicineName);$i++){
            Medicine::create([
                'prescription_id'=>$prescriptionId,
                'medicine_name'=>$medicineName[$i],
                'meal_time'=>$meal[$i],
                'morning'=>$morning[$i],
                'evening'=>$evening[$i],
                'night'=>$night[$i],
            ]);
        }
        return redirect()->route('prescription.prescriptionDetails',['prescriptionId'=>$prescriptionId])->with('status','Presctiption Make Succefully Done!!!');
    }

    #Get Prescription detalils
    public function GetPrescriptionDetails($precriptionId){
        $prescriptionDetails = Prescription::with('PrescriptionMedicine')->where(['id'=>$precriptionId])->first();
        $reportDetails = Report::with('patient')->where(['id'=>$prescriptionDetails->report_id])->first();

        return view('prescription.prescriptionDetails',['prescriptionDetails'=>$prescriptionDetails,'reportDetails'=>$reportDetails]);
    }

    #Get Prescription list by report id
    public function GetPrescriptionListByReport($reportId){

        $prescriptionList= DB::table('prescriptions')
            ->select('prescriptions.id','prescriptions.date','prescriptions.time','patients.name','patients.mobile')
            ->join('reports', 'prescriptions.report_id', '=', 'reports.id')
            ->join('patients', 'reports.patient_id', '=', 'patients.id')
            ->where('reports.id',$reportId)
            ->get();
        if($prescriptionList->count()==0){
            Session::flash('message', 'Make the first prescription....');
            return redirect()->back()->with('message', 'Make the first prescription....');
        }
        return view('prescription.prescriptionList',['prescriptionList'=>$prescriptionList]);
    }

    #Get All prescription list
    public function GetPrescriptionList(){
        $prescriptionList= DB::table('prescriptions')
                            ->select('prescriptions.id','prescriptions.date','prescriptions.time','patients.name','patients.mobile')
                            ->join('reports', 'prescriptions.report_id', '=', 'reports.id')
                            ->join('patients', 'reports.patient_id', '=', 'patients.id')

                            ->get();

        return view('prescription.prescriptionList',['prescriptionList'=>$prescriptionList]);
    }

    /*Download prescription pdf*/
    public function DownloadPrescription($precriptionId){
        $prescriptionDetails = Prescription::with('PrescriptionMedicine')->where(['id'=>$precriptionId])->first();
        $reportDetails = Report::with('patient')->where(['id'=>$prescriptionDetails->report_id])->first();

        //return view('pdf.prescription',['prescriptionDetails'=>$prescriptionDetails,'reportDetails'=>$reportDetails]);

        $pdf = PDF::loadView('pdf.prescription',['prescriptionDetails'=>$prescriptionDetails,'reportDetails'=>$reportDetails]);
        return $pdf->stream('document.pdf');
    }

    #Search medicine for live search by ajax request
    public function SearchMedicine(Request $request){
        $key = $request->input('value');
        $medicineList = ExportImport::limit(4)->where('medicine_name', 'LIKE', '%'.$key.'%')->get()->unique();
        if($medicineList->count()>0){
            echo "<ul class='search-result'>";
            foreach ($medicineList as $medicine){
                echo    '<li class="result-list"><span class="medicineName">'.$medicine->medicine_name.'</span></li>';
               /* echo '<li>'.
                        '<td>'.$medicine->medicine_name.'</td>'.

                        '<td>'.$medicine->contains.'</td>'.

                        '<td>'.$medicine->dosage_form.'</td>'.

                        '<td>'.$medicine->manufacturer.'</td>'.
                    '</li>';*/
            }
            echo "</ul>";
        }


    }
}
