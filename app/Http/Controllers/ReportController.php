<?php

namespace App\Http\Controllers;

use App\Advice;
use App\ChiefComplain;
use App\Examination;
use App\MedicalHistroy;
use App\Patient;
use App\Report;
use App\ReportAdvice;
use App\ReportComplains;
use App\ReportExamination;
use App\ReportMedicalHistory;
use App\ReportTeethLocation;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    #get report view
    public function GetReportView($patientId){
        $patientDetail =Patient::where('id',$patientId)->first();

        if($patientDetail==null){
            return redirect()->route('patient.getPatientList');
        }

        $chiefComplain = ChiefComplain::all();
        $examination = Examination::all();
        $medicalHistory = MedicalHistroy::all();
        $adviceList = Advice::all();
        return view('report.addReport',[
            'chiefComplain'=>$chiefComplain,
            'examination'=>$examination,
            'medicalHistory'=>$medicalHistory,
            'adviceList'=>$adviceList,
            'patientDetail'=>$patientDetail
        ]);
    }

    #post req add report
    public function PostReportAdd(Request $request){
       //dd($request->all());
        $this->validate($request,[
            'patientId'=>'required|numeric',
            'chief_complain'=>'required|array',
            'locationOne'=>'required|array',
            'examination'=>'required|array',
            'locationTwo'=>'required|array',
            'medicalHistory'=>'required|array',
            'advice'=>'required|array',
        ],[
            'patientId.required'=>'Patient does not set',
            'patientId.numeric'=>'Patient data is invalid',
            'chief_complain.required'=>'Please select chief complain',
            'locationOne.required'=>'Primary location invalid',
            'examination.required'=>'Examination invalid',
            'locationTwo.required'=>'Secondary location invalid',
            'medicalHistory.required'=>'Medical history invalid',
            'advice.required'=>'Advice invalid',
        ]);

        $patientId = $request->input('patientId');
        $chiefComplainList = $request->input('chief_complain');
        $locationOneList = $request->input('locationOne');
        $examinationList = $request->input('examination');
        $locationTwoList = $request->input('locationTwo');
        $medicalHistoryList = $request->input('medicalHistory');
        $adviceList = $request->input('advice');
        $report = Report::create([
                    'patient_id'=>$patientId,
                    'report_create_date'=>date('Y-m-d'),
                    'report_create_time'=>date('h:i A'),
                ]);
        $insertedId = $report->id;
        foreach ($adviceList as $advice){
            ReportAdvice::create(['report_id'=>$insertedId,'advice_id'=>$advice]);
        }
        foreach ($chiefComplainList as $chiefComplain){
            ReportComplains::create(['report_id'=>$insertedId,'complain_id'=>$chiefComplain]);
        }
        foreach ($examinationList as $examination){
            ReportExamination::create(['report_id'=>$insertedId,'examination_id'=>$examination]);
        }
        foreach ($locationTwoList as $location){
            ReportTeethLocation::create(['report_id'=>$insertedId,'teeth_location_id'=>$location]);
        }
        foreach ($medicalHistoryList as $medicalHistory){
            ReportMedicalHistory::create(['report_id'=>$insertedId,'medical_history_id'=>$medicalHistory]);
        }

        return redirect()->route('report.reportDetails',['reportId'=>$insertedId]);
    }

    #Show report details by report id
    public function GetReportDetails($reportId){
        $reportDetails = Report::with(['patient','ReportChiefComplain','ReportExamination','ReportSecondayLocation','ReportMedicalHistory','ReportAdvice'])->where(['id'=>$reportId])->first();

        $chiefComplain = ChiefComplain::all();
        $examination = Examination::all();
        $medicalHistory = MedicalHistroy::all();
        $adviceList = Advice::all();
        return view('report.reportDetails',
            [
                'reportDetails'=>$reportDetails,'chiefComplain'=>$chiefComplain,
                'examination'=>$examination,
                'medicalHistory'=>$medicalHistory,
                'adviceList'=>$adviceList,
                'reportId'=>$reportId
            ]);
    }

    #get report list
    public function GetReportList(){
        $reportList = Report::with('patient')->get();
      //  dd($reportList);
        return view('report.reportList',['reportList'=>$reportList]);
    }
}
