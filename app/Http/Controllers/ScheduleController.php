<?php

namespace App\Http\Controllers;

use App\WorkSchedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    public function addNewSchedule($reportId,Request $request){


        $this->validate($request,[
            'work'=>'array',
            'work.*'=>'required',
            'description.*'=>'required',
            'description'=>'required|array',
            'appointment'=>'required|array',
            'appointment.*'=>'required|date_format:"d-m-Y"|after:'.date('d-m-Y')

        ],[
            'work.*.required'=>'Work is required',
            'work.array'=>'Work is invalid',
            'description.*.required'=>'Description is required',
            'description.array'=>'Description is invalid',
            'appointment.array'=>'Appointment date is invalid',
            'appointment.*.required'=>'Appointment date is required',
            'appointment.*.after'=>'Appointment date is must be after today',
        ]);
        $work = $request->input('work');
        $description = $request->input('description');
        $appointment = $request->input('appointment');
        for ($i=0;$i<sizeof($work);$i++){
            WorkSchedule::create([
                'work'=>$work[$i],
                'description'=>$description[$i],
                'date'=>$appointment[$i],
                'report_id'=>$reportId,
            ]);
        }
        return redirect()->back()->with('status','Work Schedule Create Successfully Done');
    }
}
