<?php

namespace App\Http\Controllers;

use App\Advice;
use App\MedicalHistroy;
use App\TreatmentCost;
use Session;
use App\ChiefComplain;
use App\Examination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SetupController extends Controller
{
    #get Chief Complian list
    public function GetChiefComplain(){
        $complainList = ChiefComplain::all();
        return view('setup.chiefComplain',['complainList'=>$complainList]);
    }

    #post chief complian add
    public function PostChiefComplain(Request $request){
        $this->validate($request,[
            'complainName'=>'required|string|min:1|unique:chief_complains,complain_name',
        ],[
            'complainName.required'=>'Chief Complain Name is required',
            'complainName.unique'=>'Chief Complain Name has already been taken',
            'name.string'=>'Chief Complain Name is invalid',
        ]);
        $complainName =$request->input('complainName');
        ChiefComplain::create(['complain_name'=>$complainName]);
        return Redirect::back()->with('message', 'The Chief Complain Name added success');
    }

    /*Update chief complain by it's id*/
    public function UpdateChiefComplain(Request $request){

        $validator = \Validator::make($request->all(),[
            'complainId'=>'required|min:1',
            'complainName'=>'required|string|min:1|unique:chief_complains,complain_name',
        ],[
            'complainName.required'=>'Chief Complain Name is required',
            'complainName.unique'=>'Chief Complain Name has already been taken',
            'complainId.required'=>'Chief Complain update invalid request',
        ]);

        if ($validator->fails()) {
            echo "<div class='alert alert-danger text-center'>";
            foreach ($validator->messages()->getMessages() as $field_name => $messages)
            {
                foreach ($messages as $mes){
                    echo $mes."<br/>";
                }
            }
            echo "</div>";
            die();
        }else{
            ChiefComplain::where(['id'=>$request->input('complainId')])->update(['complain_name'=>$request->input('complainName')]);
            echo null;
        }
    }

    /*Delete complain by it's id*/
    public function DeleteChiefComplain(Request $request){
        $chiefComplain = ChiefComplain::where(['id'=>$request->input('complianId')])->first();
        $chiefComplain->delete();
    }


    /*Get examination list*/
    public function GetExamination(){
        $examinationList = Examination::all();
        return view('setup.examination',['examinationList'=>$examinationList]);
    }

    /*Update examination name by it's ID*/
    public function CreateUpdateExamination(Request $request){
        $validator = \Validator::make($request->all(),[
            'examinationId'=>'required|min:1',
            'examinationName'=>'required|string|min:1|unique:examinations,examination_name',
        ],[
            'examinationName.required'=>'Examination Name is required',
            'examinationName.unique'=>'Examination Name has already been taken',
            'examinationId.required'=>'Examination invalid update request',
        ]);

        if ($validator->fails()) {
            echo "<div class='alert alert-danger text-center'>";
            foreach ($validator->messages()->getMessages() as $field_name => $messages)
            {
                foreach ($messages as $mes){
                    echo $mes."<br/>";
                }
            }
            echo "</div>";
            die();
        }else{
            $object = Examination::updateOrCreate(
                        ['id'=>$request->input('examinationId')],
                        ['examination_name'=>$request->input('examinationName')]
                    );

            $id = $object->id;
            //session(['examId' => $id]);
            session()->put('dataId', $id);

            echo null;
        }
    }

    /*Delete examination by it's id*/
    public function DeleteExamination(Request $request){
        $chiefComplain = Examination::where(['id'=>$request->input('examinationId')])->first();
        $chiefComplain->delete();
    }

    //Get session Data
    public function GetSessionData(){
        echo session()->get("dataId");
    }

    /*Get mediacl history list*/
    public function GetMedicalHistory(){
        $historyList = MedicalHistroy::all();
        return view('setup.history',['historyList'=>$historyList]);
    }

    /*Add medical history/problem by ajax*/
    public function AddMedicalHistory(Request $request){
        $validator = \Validator::make($request->all(),[
            'dataId'=>'required|min:1',
            'dataName'=>'required|string|min:1|unique:medical_histories,disease_name',
        ],[
            'dataName.required'=>'Medical history/problem name is required',
            'dataName.unique'=>'Medical history/problem name has already been taken',
            'dataName.required'=>'Medical history/problem invalid update request',
        ]);

        if ($validator->fails()) {
            echo "<div class='alert alert-danger text-center'>";
            foreach ($validator->messages()->getMessages() as $field_name => $messages)
            {
                foreach ($messages as $mes){
                    echo $mes."<br/>";
                }
            }
            echo "</div>";
            die();
        }else{
            $object = MedicalHistroy::updateOrCreate(
                ['id'=>$request->input('dataId')],
                ['disease_name'=>$request->input('dataName')]
            );

            $id = $object->id;
            //session(['examId' => $id]);
            session()->put('dataId', $id);

            echo null;
        }
    }

    /*Delete Medical history/problem by ajax*/
    public function DeleteMedicalHistory(Request $request){
        $history = MedicalHistroy::where(['id'=>$request->input('deleteId')])->first();
        $history->delete();
    }

    /*Get advice list*/
    public function GetAdvice(){
        $adviceList = Advice::all();
        return view('setup.advice',['adviceList'=>$adviceList]);
    }

    /*Add/Update Advice by Ajax*/
    public  function AddUpdateAdvice(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'dataId' => 'required|min:1',
            'dataName' => 'required|string|min:1|unique:advices,advice_name',
        ], [
            'dataName.required' => 'Advice name is required',
            'dataName.unique' => 'Advice name has already been taken',
            'dataId.required' => 'Advice invalid update request',
        ]);

        if ($validator->fails()) {
            echo "<div class='alert alert-danger text-center'>";
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                foreach ($messages as $mes) {
                    echo $mes . "<br/>";
                }
            }
            echo "</div>";
            die();
        } else {
            $object = Advice::updateOrCreate(
                ['id' => $request->input('dataId')],
                ['advice_name' => $request->input('dataName')]
            );

            $id = $object->id;
            //session(['examId' => $id]);
            session()->put('dataId', $id);

            echo null;
        }
    }

    /*Delete Advice Data*/
    public function DeleteAdviceData(Request $request){
        $advice = Advice::where(['id'=>$request->input('deleteId')])->first();
        $advice->delete();
    }

    /*Treatment Get all data*/
    public function GetTreatmentCost(){
        $treatmentList = TreatmentCost::all();
        return view('setup.treatmentCost',['treatmentList'=>$treatmentList]);
    }

    /*Add treatment name and cost*/
    public function AddEditTreatmentCost(Request $request){

        $validator = \Validator::make($request->all(), [
            'dataId' => 'required|min:1',
            'dataName' => 'required|string|min:1|unique:treatment_costs,treatment_name,'.$request->input('dataId'),
            'dataCost' => 'required|numeric|min:1',
        ], [
            'dataName.required' => 'Treatment name is required',
            'dataName.unique' => 'Treatment name has already been taken',
            'dataCost.required' => 'Treatment cost is required',
            'dataCost.numeric' => 'Treatment cost is invalid, must be number',
            'dataId.required' => 'Treatment invalid update request',
        ]);

        if ($validator->fails()) {
            echo "<div class='alert alert-danger text-center'>";
            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                foreach ($messages as $mes) {
                    echo $mes . "<br/>";
                }
            }
            echo "</div>";
            die();
        } else {
            $object = TreatmentCost::updateOrCreate(
                ['id' => $request->input('dataId')],
                [
                    'treatment_name' => $request->input('dataName'),
                    'treatment_cost' => $request->input('dataCost')
                ]
            );

            $id = $object->id;
            //session(['examId' => $id]);
            session()->put('dataId', $id);

            echo null;
        }
    }

    /*Delete treatment */
    public function DeleteTreatmentCost(Request $request){
        $treatmentDetails = TreatmentCost::where(['id'=>$request->input('deleteId')])->first();
        $treatmentDetails->delete();

    }


}
