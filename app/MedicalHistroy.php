<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalHistroy extends Model
{
    protected $table = 'medical_histories';

    protected $fillable = ['disease_name'];
}
