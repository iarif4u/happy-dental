<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicine extends Model
{
    protected $table = 'medicines';
    protected $fillable = ['prescription_id','medicine_name','meal_time','morning','evening','night'];
}
