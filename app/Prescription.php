<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    protected $table = 'prescriptions';

    protected $fillable = ['report_id','date','time','c_c','o_e','advice'];


    #Prescription has many Medicine
    public function PrescriptionMedicine(){
        return $this->hasMany('App\Medicine','prescription_id','id');
    }

    #Prescription has many Medicine
    public function PrescriptionReport(){
        return $this->belongsTo('App\Report','report_id');
    }
}
