<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';
    protected $fillable = ['patient_id','report_create_date','report_create_time'];

    /*Report has a patient*/
    public function patient(){
        return $this->hasOne('App\Patient','id','patient_id');
    }

    #Report has many Chief Complain
    public function ReportChiefComplain(){
        return $this->hasMany('App\ReportComplains','report_id','id');
    }

    #Report has many Examination
    public function ReportExamination(){
        return $this->hasMany('App\ReportExamination','report_id','id');
    }
    #Report has many Location
    public function ReportSecondayLocation(){
        return $this->hasMany('App\ReportTeethLocation','report_id','id');
    }

    #Report has many Medical History
    public function ReportMedicalHistory(){
        return $this->hasMany('App\ReportMedicalHistory','report_id','id');
    }

    #Report has many advice
    public function ReportAdvice(){
        return $this->hasMany('App\ReportAdvice','report_id','id');
    }

    #Report has many Prescription
    public function ReportPrescription(){
        return $this->hasMany('App\Prescription','report_id','id');
    }
}
