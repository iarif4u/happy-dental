<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportAdvice extends Model
{
    protected  $table = "report_advices";
    protected $fillable = ['report_id','advice_id'];
}
