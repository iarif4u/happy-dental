<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportComplains extends Model
{
    protected $table = 'report_complains';
    protected $fillable = ['report_id','complain_id'];
}
