<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportExamination extends Model
{
    protected $table = 'report_examinations';
    protected $fillable = ['report_id','examination_id'];
}
