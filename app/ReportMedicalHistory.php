<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportMedicalHistory extends Model
{
    protected $table = 'report_medical_histories';
    protected $fillable = ['report_id','medical_history_id'];
}
