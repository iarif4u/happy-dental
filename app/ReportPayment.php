<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportPayment extends Model
{
    protected $table = 'report_payments';
    protected $fillable = ['amount','date','time','report_id'];
}
