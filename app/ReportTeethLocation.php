<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportTeethLocation extends Model
{
    protected $table = 'report_teeth_locations';
    protected $fillable = ['report_id','teeth_location_id'];
}
