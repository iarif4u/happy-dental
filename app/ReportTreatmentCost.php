<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportTreatmentCost extends Model
{
    protected $table = 'report_treatment_costs';
    protected $fillable = ['report_id','treatment_id','treatment_cost_this_time','unit','date'];


}
