<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreatmentCost extends Model
{
    protected $table = 'treatment_costs';

    protected $fillable = ['treatment_name','treatment_cost'];

}
