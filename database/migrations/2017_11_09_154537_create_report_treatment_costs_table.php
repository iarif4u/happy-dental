<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTreatmentCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_treatment_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_id');
            $table->integer('treatment_id');
            $table->integer('treatment_cost_this_time');
            $table->integer('unit');
            $table->integer('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_treatment_costs');
    }
}
