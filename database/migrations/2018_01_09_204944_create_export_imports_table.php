<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExportImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('export_imports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('medicine_name');
            $table->string('contains');
            $table->string('dosage_form');
            $table->string('manufacturer');
            $table->string('drugs_for');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('export_imports');
    }
}
