<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Doctor Login</title>

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="{{ asset('assets/dist/img/ico/favicon.png')}}" type="image/x-icon">


    <!-- Bootstrap -->
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap rtl -->
    <!--<link href="{{ asset('assets/bootstrap-rtl/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css"/>-->
    <!-- Pe-icon-7-stroke -->
    <link href="{{ asset('assets/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}" rel="stylesheet" type="text/css"/>
    <!-- style css -->
    <link href="{{ asset('assets/dist/css/stylehealth.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Theme style rtl -->
    <!--<link href="{{ asset('assets/dist/css/stylehealth-rtl.css')}}" rel="stylesheet" type="text/css"/>-->
    <style>
        .container-center {
            max-width: 450px;
            margin: 10% auto 0;
            padding: 20px;
        }
    </style>
</head>
<body>
<!-- Content Wrapper -->
<div class="login-wrapper">

    <div class="container-center">
        <div class="panel panel-bd">
            <div class="panel-heading">
                <div class="view-header">

                    <div class="header-title">
                        <h3>Admin Login</h3>

                    </div>
                </div>
            </div>
            <div class="panel-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p class="each-error">{{ $error }} </p>
                        @endforeach
                    </div>
                @endif
                <form method="post" action="{{route('login')}}" id="loginForm" novalidate>
                    {{csrf_field()}}
                    <div class="form-group">
                        <label class="control-label" for="email">Email</label>
                        <input type="text" placeholder="example@gmail.com" title="Please enter you username" required="" value="" name="email" id="username" class="form-control">
                        <span class="help-block small">Your unique username to app</span>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="password">Password</label>
                        <input type="password" title="Please enter your password" placeholder="******" required="" value="" name="password" id="password" class="form-control">
                        <span class="help-block small">Your strong password</span>
                    </div>
                    <div>
                        <button  type="submit" class="btn btn-primary">Login</button>
                       {{-- <a class="btn btn-warning" href="register.html">Register</a>--}}
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.content-wrapper -->
<!-- jQuery -->
<script src="{{ asset('assets/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
<!-- bootstrap js -->
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
</body>
</html>