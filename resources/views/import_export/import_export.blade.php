<?php
/**
 * Created by PhpStorm.
 * User: Arif
 * Date: 10/30/2017
 * Time: 8:40 PM
 */
?>
@extends('layouts.master')

@section('content')
    <!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">


            <div class="header-icon">
                <i class="pe-7s-note2"></i>
            </div>
            <div class="header-title">
                <h1>Import Export </h1>
                <small>Medicine List</small>
                <ol class="breadcrumb hidden-xs">
                    <li><a href="{{route('home')}}"><i class="pe-7s-home"></i> Home</a></li>
                    <li class="active">Import & Export</li>
                </ol>
            </div>
        </section>
        <!-- Main content -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- Form controls -->
                <div class="col-sm-12">
                    <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                            <div class="btn-group">
                                <a class="btn btn-primary" href="javascript:void(0);"> <i class="fa fa-list"></i>
                                    Import & Export
                                </a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                                <script>
                                    // notification
                                    Notification('Patient Add Success');
                                </script>
                            @endif
                            @if(count($errors) > 0)
                                <div class="alert">
                                    @foreach ($errors->all() as $error)
                                        <p class="each-error">{{ $error }} </p>
                                    @endforeach
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-4">
                                    <button class="btn btn-default">Export</button>
                                </div>
                                <div class="col-md-8">
                                    <nav class="">
                                        <a class="btn btn-primary" href="{{route('importExport.downloadExcel',['type'=>'CSV'])}}">CSV</a>
                                        <a class="btn btn-primary" href="{{route('importExport.downloadExcel',['type'=>'XLS'])}}">XLS</a>
                                        <a class="btn btn-primary" href="{{route('importExport.downloadExcel',['type'=>'XLSX'])}}">XLSX</a>
                                    </nav>
                                </div>
                            </div>
                            <div class="msg margin-top-15"></div>
                            <div class="col-sm-offset-2 col-sm-8 form-group">
                                <form method="post" id="excelFile" enctype="multipart/form-data" action="{{route('importExport.uploadExcel')}}" class="dropzone">
                                    {{csrf_field()}}

                                  {{--  <input type="file" name="file" id="" class="form-control">--}}
                                    <div class="fallback">

                                    </div>
                                    <div>

                                        <h3 class="text-center">Upload Excel File By Click On Box</h3>

                                    </div>
                                   {{-- <input type="submit" value="Submit" class="btn btn-success">--}}
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </section> <!-- /.content -->
    </div> <!-- /.content-wrapper -->
@endsection


@section('script')
    <script src="{{ asset('assets/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ asset('assets/dropzone/dropzone-amd-module.min.js') }}"></script>
    <script type="text/javascript">
        /*Active Menu from sidebar by add "active" Class*/
        $('span:contains("Export / Import")').parent().parent().addClass('active');
    </script>
    {{--Intialize dropzone--}}
    <script type="text/javascript">
        Dropzone.options.excelFile = {
            maxFilesize  : 5,
            parallelUploads: 1,
            acceptedFiles: ".csv,.xls,.xlsx",
            success: function(file, response){
                console.log('WE NEVER REACH THIS POINT.');
                $('.msg').html(response);
            },
            error: function(file, response) {
                var error = '<div class="alert alert-danger">Import does not completed</div>';
                $('.msg').html(error);
            }
        };
    </script>
@endsection