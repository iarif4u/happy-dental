<?php
/**
 * Created by PhpStorm.
 * User: Arif
 * Date: 10/30/2017
 * Time: 3:24 PM
 */
?>
<footer class="main-footer">
    <div class="pull-right hidden-xs"> <b>Version</b> 1.0</div>
    <strong>Copyright &copy; 2016-2017 <a href="#">DifferentCoder</a>.</strong> All rights reserved.
</footer>
</div> <!-- ./wrapper -->
<!-- ./wrapper -->
<!-- Start Core Plugins
=====================================================================-->
<!-- jQuery -->
<script src="{{ asset('assets/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
<!-- jquery-ui -->
<script src="{{ asset('assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<!-- lobipanel -->
<script src="{{ asset('assets/plugins/lobipanel/lobipanel.min.js')}}" type="text/javascript"></script>

{{--modal--}}
<script src="{{ asset('assets/plugins/modals/modalEffects.js')}}" type="text/javascript"></script>
<!-- Pace js -->
<script src="{{ asset('assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="{{ asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset('assets/plugins/fastclick/fastclick.min.js')}}" type="text/javascript"></script>

<!-- Hadmin frame -->
<script src="{{ asset('assets/dist/js/custom1.js')}}" type="text/javascript"></script>
<!-- End Core Plugins
=====================================================================-->
<!-- Start Page Lavel Plugins
=====================================================================-->
<!-- Toastr js -->
<script src="{{ asset('assets/plugins/toastr/toastr.min.js')}}" type="text/javascript"></script>
<!-- Sparkline js -->
<script src="{{ asset('assets/plugins/sparkline/sparkline.min.js')}}" type="text/javascript"></script>
<!-- Data maps js -->
<script src="{{ asset('assets/plugins/datamaps/d3.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/datamaps/topojson.min.js')}}" type="text/javascript"></script>

<!-- Counter js -->
<script src="{{ asset('assets/plugins/counterup/waypoints.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
<!-- ChartJs JavaScript -->
<script src="{{ asset('assets/plugins/chartJs/Chart.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/emojionearea/emojionearea.min.js')}}" type="text/javascript"></script>
<!-- Monthly js -->
<script src="{{ asset('assets/plugins/monthly/monthly.js')}}" type="text/javascript"></script>
<!-- Data maps -->
<script src="{{ asset('assets/plugins/datamaps/d3.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/datamaps/topojson.min.js')}}" type="text/javascript"></script>




<!-- End Page Lavel Plugins
=====================================================================-->
<!-- Start Theme label Script
=====================================================================-->
<!-- Dashboard js -->
<script src="{{ asset('assets/dist/js/custom.js')}}" type="text/javascript"></script>

<!-- End Theme label Script
=====================================================================-->
<script>
    function nextDate(myDate,separator,incrementDate){
        var sDate =myDate.split(separator)[0];
        var sMonth =myDate.split(separator)[1];
        var sYear=myDate.split(separator)[2];
        var sFullDate = sMonth+separator+sDate+separator+sYear;
        var date = new Date(sFullDate);
        date.setDate(date.getDate()+1);
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var y = date.getFullYear();
        alert(dd);
    }
    /*Script start for datepicker*/
    $(document).ready(function(){
        var lastDate = 1;
        $(document).on('click', '.datepicker', function(){
            $(this).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                minDate: lastDate,
                maxDate: '+3M',
                onSelect: function(dateText) {
                    lastDate = dateText;
               /*     nextDate(dateText,"-","1");*/
                }
            }).focus();
            $(this).removeClass('datepicker');
        });
    });
    /*Script end for datepicker*/

    function clearInputField(){
        $("#dataName").val('');
        $("#dataCost").val('');
    }
    /*Show notification*/
    function Notification(Message) {
        setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 2000
            };
            toastr.success('Notification', Message);
        }, 200);
    }


</script>

<script src="{{ asset('assets/data-table/dataTables.min.js')}}" type="text/javascript"></script>

