<?php
/**
 * Created by PhpStorm.
 * User: Arif
 * Date: 10/30/2017
 * Time: 3:25 PM
 */
?>
<!-- =============================================== -->
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="image pull-left">
                <img src="{{ asset('assets/dist/img/avatar5.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="info">
                <h4>Welcome</h4>
                <p>Mr. Alrazy</p>
            </div>
        </div>

        <!-- sidebar menu -->
        <ul class="sidebar-menu">
            <li>
                <a href="{{route('home')}}"><i class="fa fa-hospital-o"></i><span>Dashboard</span>
                </a>
            </li>

            <li class="treeview">
                <a href="javascript:void(0);">
                    <i class="fa fa-user"></i><span>Patient</span>
                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('patient.patientAdd')}}">Add patient</a></li>
                    <li><a href="{{route('patient.getPatientList')}}">Patient list</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="javascript:void(0);">
                    <i class="fa fa-sitemap"></i><span>Report/Prescription</span>
                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('report.list')}}">View Report</a></li>
                    <li><a href="{{route('prescription.prescriptionList')}}">View Prescription</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="javascript:void(0);">
                    <i class="fa fa-list-alt"></i> <span>Payment</span>
                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="TreatmentCost.php">Treatment Cost</a></li>
                    <li><a href="PaymentSchedule.php">Payment Schedule</a></li>

                </ul>
            </li>
            <li class="treeview">
                <a href="javascript:void(0);">
                    <i class="glyphicon glyphicon-wrench"></i> <span>Setup</span>
                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('setup.chiefComplain')}}">Chief Complain</a></li>
                    <li><a href="{{route('setup.getExamination')}}">Examination</a></li>
                    <li><a href="{{route('setup.history')}}">Medical History/Problem</a></li>
                    <li><a href="{{route('setup.advice')}}">Advice</a></li>
                    <li><a href="{{route('setup.treatmentCost')}}">Treatment Cost</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{route('importExport.getView')}}">
                    <i class="fa fa-list-alt"></i> <span>Export / Import</span>

                </a>

            </li>
        </ul>
    </div> <!-- /.sidebar -->
</aside>
