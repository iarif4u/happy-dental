<?php
/**
 * Created by PhpStorm.
 * User: Arif
 * Date: 10/30/2017
 * Time: 8:40 PM
 */
?>
@extends('layouts.master')

@section('content')



<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <form action="#" method="post" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
            {{csrf_field()}}
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
            </div>
        </form>

        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1> Add Patient </h1>
            <small> Patient Information</small>
            <ol class="breadcrumb hidden-xs">
                <li><a href="home.php"><i class="pe-7s-home"></i> Home</a></li>
                <li class="active">Add Patient</li>
            </ol>
        </div>
    </section>
    <!-- Main content -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Form controls -->
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="btn-group">
                            <a class="btn btn-primary" href="javascript:void(0);"> <i class="fa fa-list"></i>  Patient Add </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            <script>
                                // notification
                                Notification('Patient Add Success');
                            </script>
                        @endif
                        @if(count($errors) > 0)
                            <div class="alert">
                                @foreach ($errors->all() as $error)
                                    <p class="each-error">{{ $error }} </p>
                                @endforeach
                            </div>
                        @endif
                            {{ Form::open(['route' => 'patient.patientAdd','method' => 'post','class'=>"col-sm-12"])}}
                            {{csrf_field()}}
                            <div class="col-sm-6 form-group">
                                {{Form::label('name', 'Patient Name', ['class' => 'awesome'])}}

                                {{Form::text('name', null,['class'=>"form-control",'placeholder'=>'Enter Patient Name'])}}
                            </div>
                            <div class="col-sm-6 form-group">
                                {{Form::label('moble', 'Mobile', ['class' => 'awesome'])}}

                                {{Form::text('mobile', null,['class'=>"form-control",'placeholder'=>'Enter Mobile Number'])}}
                            </div>
                            <div class="col-sm-6 form-group">
                                {{Form::label('age', 'Age', ['class' => 'awesome'])}}

                                {{Form::text('age', null,['class'=>"form-control",'placeholder'=>'Enter Age'])}}
                            </div>
                            <div class="col-sm-6 form-group">
                                {{Form::label('sex', 'Gender', ['class' => 'awesome'])}}
                                        {{Form::select('sex', ['0'=>'Select One','Male' => 'Male', 'Female' => 'Female'],'0',['class'=>'form-control'])}}
                            </div>
                            <div class="col-sm-12 form-group">
                                <label>Address</label>
                                {!! Form::textarea('address',null,['class'=>'form-control', 'placeholder'=>'Enter Address ...','rows' => 3, 'cols' => 30]) !!}
                            </div>

                            <div class="col-sm-12 reset-button">

                                {{--<input type="submit" class="btn btn-success" value="Save">--}}
                                {{Form::reset('Reset',['class'=>'btn btn-danger'])}}
                                {{Form::submit('Save',['class'=>'btn btn-success'])}}

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </section> <!-- /.content -->
</div> <!-- /.content-wrapper -->
@endsection


@section('script')
    <script type="text/javascript">
        /*Active Menu from sidebar by add "active" Class*/
        $('span:contains("Patient")').parent().parent().addClass('active');
        $('a:contains("Add patient")').parent().addClass('active');
    </script>
@endsection