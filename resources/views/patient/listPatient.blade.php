<?php
/**
 * Created by PhpStorm.
 * User: Arif
 * Date: 10/30/2017
 * Time: 10:24 PM
 */
?>
@extends('layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                </div>
            </form>

            <div class="header-icon">
                <i class="pe-7s-note2"></i>
            </div>
            <div class="header-title">
                <h1>Patient </h1>
                <small> Patient List Infromation</small>
                <ol class="breadcrumb hidden-xs">
                    <li><a href="{{route('home')}}"><i class="pe-7s-home"></i> Home</a></li>
                    <li class="active">Patient List info</li>
                </ol>
            </div>
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="resultAjax"></div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                            <div class="btn-group">
                                <p class="text-success">Patient List</p>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="panel-body">
                                <div id="example_wrapper" class="dataTables_wrapper">

                                    <table id="example" class="display nowrap dataTable dtr-inline" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 136px;">Name</th>
                                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 216px;">Mobile</th>
                                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 103px;">Age</th>
                                            <th class="dt-body-right sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 42px;">Gender</th>
                                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 91px;">Address</th>
                                            <th class="dt-body-right sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 79px;">Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>

                                        <tr>
                                            <th rowspan="1" colspan="1">Name</th>
                                            <th rowspan="1" colspan="1">Mobile</th>
                                            <th rowspan="1" colspan="1">Age</th>
                                            <th class="dt-body-right" rowspan="1" colspan="1">Gender</th>
                                            <th rowspan="1" colspan="1">Address</th>
                                            <th class="dt-body-right" rowspan="1" colspan="1">Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php
                                            $i =0;
                                        ?>
                                        @foreach($patientList as $patient)

                                        <tr role="row" @if($i%2==0) class="odd patient patientRow{{$patient->id}}" @else class="even patient patientRow{{$patient->id}}" @endif>
                                            <td class="sorting_1 pName{{$patient->id}}">{{$patient->name}}</td>
                                            <td class="pMobile{{$patient->id}}">{{$patient->mobile}}</td>
                                            <td class="pAge{{$patient->id}}">{{$patient->age}}</td>
                                            <td class="dt-body-right pSex{{$patient->id}}">{{$patient->sex}}</td>
                                            <td class="pAddress{{$patient->id}}">{{$patient->address}}</td>
                                            <td class="dt-body-right">
                                                <a href="{{route('report.toPatient',with(['patientId'=>$patient->id]))}}" >

                                                <span class="icon-elemet icon_box">

                                                        <i class="hvr-buzz-out fa fa-user-plus"></i>
                                                    </span>
                                                </a>
                                                
                                                <span class="icon-elemet icon_box">
                                                <i onclick="patientEdit({{$patient->id}})" class="hvr-buzz-out fa fa-edit"></i>
                                                    </span>
                                                <span class="icon-elemet icon_box">
                                                    <i onclick="patientDelete({{$patient->id}})" class="hvr-buzz-out fa fa-trash has-error"></i>
                                                     <span class="icon-elemet">


                                            </td>
                                        </tr>
                                            <?php $i++ ?>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </section> <!-- /.content -->

        {{--Edit modal start--}}
        <button id="editModalBtn" data-toggle="modal" data-target="#editModal" class="hidden">

        </button>

        <div id="editModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->

                <div class="modal-content">
                    <div class="modal-header bg-danger bg-green">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title color-white">Udate Patient Info</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="patientName">Patient Name</label>
                            <input type="text" id="patientName" placeholder="Patient Name" class="form-control">
                            <input type="hidden" id="patientId">
                        </div>
                        <div class="form-group">
                            <label for="mobileNumber">Mobile Number</label>
                            <input type="text" id="mobileNumber" placeholder="Mobile Number" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="age">Age</label>
                            <input type="text" id="age" placeholder="Patient Age" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="sex">Gender</label>
                            <select class="form-control" id="sex" name="sex">
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea id="address" class="form-control" placeholder="Enter Address ..." rows="3" cols="30" name="address"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default closer" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-danger" onclick="editData()">Update</button>

                    </div>
                </div>
            </div>
        </div>
        {{--End modal--}}

        {{--Edit modal End--}}
        {{--Delete modal Start--}}
    <!-- Modal -->

        <button id="openDeleteModal" data-toggle="modal" data-target="#deleteModal" class="hidden">

        </button>

        <div id="deleteModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->

                <div class="modal-content">
                    <div class="modal-header bg-danger bg-red">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title color-white">Warning</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="complainId">
                        <h3>Are you Sure???
                        </h3>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default closer" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-danger" onclick="deleteData()">Yes</button>

                    </div>
                </div>
                </form>

            </div>
        </div>
        {{--End modal--}}

        {{--Delete modal End--}}



    </div> <!-- /.content-wrapper -->


    </div> <!-- /.content-wrapper -->


@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        /*Add dataTabe of Patient list*/
        $('#example').DataTable();

        $('.patient').children().addClass('width-50');

    })

    /*Patient edit modal open*/
    function patientEdit(patientId) {
        $("#editModalBtn").click();
        var name = $('.pName'+patientId).text();
        var mobile = $('.pMobile'+patientId).text();
        var age = $('.pAge'+patientId).text();
        var sex = $('.pSex'+patientId).text();
        var address = $('.pAddress'+patientId).text();

        $("#patientId").val(patientId);
        $("#patientName").val(name);
        $("#mobileNumber").val(mobile);
        $("#age").val(age);
        $("#sex").val(sex);
        $("#address").val(address);

    }

    /*Send ajax request to update patient data*/
    function editData(){
        var patientId = $("#patientId").val();
        var name = $("#patientName").val();
        var mobile = $("#mobileNumber").val();
        var age = $("#age").val();
        var sex = $("#sex").val();
        var address = $("#address").val();

        /*Send ajax request to update data*/
        $.ajax({
            url:'{{route('patient.patientEdit')}}',
            type : 'POST',
            data:{'_token':'{{csrf_token()}}','patientId':patientId,'name':name,'mobile':mobile,'age':age,'sex':sex,'address':address},
            success: function(result){
                if (!$.trim(result)) {
                    var rowData = ' <div class="alert alert-success">\n' +
                        'Patient data update' +
                        '                            </div>';

                    $('.resultAjax').html(rowData);

                    $(".pName"+patientId+"").fadeOut(function() {
                        $(this).text(name)
                    }).fadeIn();

                    $(".pMobile"+patientId+"").fadeOut(function() {
                        $(this).text(mobile)
                    }).fadeIn();

                    $(".pAge"+patientId+"").fadeOut(function() {
                        $(this).text(age)
                    }).fadeIn();

                    $(".pSex"+patientId+"").fadeOut(function() {
                        $(this).text(sex)
                    }).fadeIn();

                    $(".pAddress"+patientId+"").fadeOut(function() {
                        $(this).text(address)
                    }).fadeIn();

                    Notification("Patient Data Update Success");
                }
                else {
                    $('.resultAjax').html(result);
                }
            }});

        /*End ajax jQuery function*/
        $('.closer').click();
    }
    /*Opend delete button*/
    function patientDelete(patientId) {
        $("#openDeleteModal").click();
        $("#patientId").val(patientId);
    }

    /*Delete patient Data*/
    function deleteData() {
        var patientId = $("#patientId").val();
            /*Send ajax request to delete data*/
        $.ajax({
            url:'{{route('patient.patientDelete')}}',
            type : 'POST',
            data:{'_token':'{{csrf_token()}}','patientId':patientId},
            success: function(result){
                if (!$.trim(result)) {
                    $(".patientRow"+patientId+"").fadeOut(300, function() { $(this).remove(); });
                    Notification("Patient Data Delete Success");

                }else{
                    $('.resultAjax').html(result);
                }
            }

        });
        /*End ajax request*/
        $('.closer').click();
    }






</script>

    <script type="text/javascript">
        /*Active Menu from sidebar by add "active" Class*/
        $('span:contains("Patient")').parent().parent().addClass('active');
        $('a:contains("Patient list")').parent().addClass('active');
    </script>
@endsection