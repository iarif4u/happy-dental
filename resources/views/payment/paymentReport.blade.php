<?php
/**
 * Created by PhpStorm.
 * User: Mr. Polash
 * Date: 12/5/2017
 * Time: 8:52 PM
 */
?>

@extends('layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>

            <div class="header-icon">
                <i class="pe-7s-note2"></i>
            </div>
            <div class="header-title">
                <h1> Add Patient </h1>
                <small> Patient Infromation</small>
                <ol class="breadcrumb hidden-xs">
                    <li><a href="{{route('home')}}"><i class="pe-7s-home"></i> Home</a></li>
                    <li class="active">Payment</li>
                </ol>
            </div>
        </section>
        <!-- Main content -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- Form controls -->
                <div class="col-sm-12">
                    <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                            <div class="btn-group">
                                <h4>Patient Payment Scheduler</h4>
                                {{--<h4>{{$reportDetails->patient->name}}</h4>--}}
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                @if (session('status'))
                                    <script>
                                        // notification
                                        setTimeout(function () {
                                            toastr.options = {
                                                closeButton: true,
                                                progressBar: true,
                                                showMethod: 'slideDown',
                                                timeOut: 1000
                                            };
                                            toastr.success('Notification', 'Work Schedule Create Successfully Done');

                                        }, 2300);
                                    </script>
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                @if (session('error'))
                                        <div class="alert alert-danger">
                                            {{ session('error') }}
                                        </div>
                                    @endif
                                    @if(count($errors) > 0)
                                        <div class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <p>{{ $error }} </p>
                                            @endforeach
                                        </div>
                                    @endif
                            </div>
                            <div class="table text-bold text-uppercase text-success">
                                <div class="row">
                                    <div class="col-md-2 col-sm-6">Patient Name</div>
                                    <div class="col-md-3 col-sm-6">{{$reportDetails->patient->name}}</div>
                                    <div class="col-md-1 col-sm-6">Age</div>
                                    <div class="col-md-2 col-sm-6">{{$reportDetails->patient->age}}</div>
                                    <div class="col-md-2 col-sm-6">Report No</div>
                                    <div class="col-md-2 col-sm-6">{{$reportDetails->id}}</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 col-sm-6">Address</div>
                                    <div class="col-md-4 col-sm-6">{{$reportDetails->patient->address}}</div>
                                    <div class="col-md-2 col-sm-6 col-md-offset-2">Mobile No</div>
                                    <div class="col-md-2 col-sm-6">{{$reportDetails->patient->mobile}}</div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                @if($previousTreatmentList->count()>0)
                                    <table class="table table-bordered table-hover" cellspacing="0" width="100%" id="tblGrid">
                                        <thead>
                                        <tr>
                                            <th> No</th>
                                            <th>Name Of Treatment</th>
                                            <th>Unit</th>
                                            <th>Rate</th>
                                            <th>Cost</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $count =1;
                                            $totalCost= 0;
                                        @endphp
                                        @foreach($previousTreatmentList as $previousTreatment)
                                            @php
                                                $totalCost =$totalCost+($previousTreatment->treatment_cost_this_time *                                                                                                         $previousTreatment->unit);
                                            @endphp
                                            <tr>
                                                <td width="10px" class="ro">{{$count++}}</td>
                                                <td width="200px">
                                                    {{
                                                         $costList->where('id',$previousTreatment->treatment_id)->first()->treatment_name
                                                    }}
                                                </td>
                                                <td width="200px">
                                                    {{$previousTreatment->unit}}
                                                </td>
                                                <td width="200px">
                                                    {{$previousTreatment->treatment_cost_this_time}}
                                                </td>
                                                <td width="200px">
                                                    {{
                                                        $previousTreatment->treatment_cost_this_time*$previousTreatment->unit
                                                     }}
                                                </td>
                                                <td width="200px">
                                                    {{$previousTreatment->date}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="4" class="text-right text-bold">Total</td>
                                            <td colspan="2" class="text-bold">{{$totalCost}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="text-right text-bold">Paid</td>
                                            <td data-toggle="modal" data-target="#myModal" colspan="2" class="text-bold text-underline paid-amount">{{$paidList->sum('amount')}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="text-right text-bold">Due Amonut</td>
                                            @php
                                                $due = $totalCost-$paidList->sum('amount');
                                            @endphp
                                            <td colspan="2" class="text-bold due-amount">{{$due}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                            {{--Start modal--}}
                            <div id="myModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->

                                    <div class="modal-content">
                                        <div class="modal-header bg-green bg-success">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title color-white">Payment List</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="table-responsive">
                                                <table class="table data-table">
                                                    <thead>
                                                    <tr>
                                                        <td>Sl</td>
                                                        <td>Date</td>
                                                        <td>Time</td>
                                                        <td>Amount</td>
                                                    </tr>
                                                    </thead>

                                                    @if($paidList)
                                                        @php
                                                            $count =1;
                                                        @endphp
                                                    <tbody class="paidList">
                                                        @foreach($paidList as $paid)
                                                            <tr>
                                                                <td>{{$count++}}</td>
                                                                <td>{{$paid->date}}</td>
                                                                <td>{{$paid->time}}</td>
                                                                <td>{{$paid->amount}}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>

                                                            <td colspan="3" class="text-right">Total</td>
                                                            <td>{{$paidList->sum('amount')}}</td>
                                                        </tr>
                                                    </tfoot>
                                                    @endif

                                                </table>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default closer" data-dismiss="modal">Close</button>

                                        </div>
                                    </div>

                                </div>
                            </div>
                            {{--End modal--}}
                            <div class="table-responsive">
                                <form autocomplete="off" action="{{route('payment.postPaymentAdd',['reportId'=>$reportDetails->id])}}" method="post">
                                    {{csrf_field()}}
                                    <table class="table table-bordered table-hover" cellspacing="0" width="100%" id="tblGrid">
                                        <thead>
                                        <tr>
                                            <th> No</th>
                                            <th>Name Of Treatment</th>
                                            <th>Unit</th>
                                            <th>Rate</th>
                                            <th>Cost</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td width="10px" class="ro">1</td>
                                            <td width="200px">
                                                <select onchange="setCost()" class="form-control" name="treatment_name[]">
                                                    <option value="0">Select One</option>
                                                    @foreach($costList as $cost)
                                                        <option value="{{$cost->id}}">{{$cost->treatment_name}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td width="200px">
                                                <input value='0' onkeyup="setCost()" class="form-control unit" type="text" name="unit[]" />
                                            </td>
                                            <td width="200px">
                                                <input value='0' readonly class="form-control rate" type="text" name="rate[]" />
                                            </td>
                                            <td width="200px">
                                                <input value='0' readonly class="form-control cost" type="text" name="cost[]" /> </td>
                                            <td width="10px">
                                                <input type="button" value="X" onclick="removeRow(this);" />
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="submit-box" border="0" cellspacing="1" width="100%">
                                        <thead>
                                        <tr>
                                            <th> </th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <td width="10px"></td>
                                            <td width="200px"></td>
                                            <td width="200px"></td>
                                            <td width="190px">
                                                Total
                                            </td>
                                            <td width="220px">
                                                <input class="0" type="hidden" name="0" value="0">
                                                @foreach($costList as $cost)
                                                    <input class="{{$cost->id}}" type="hidden" name="{{$cost->id}}" value="{{$cost->treatment_cost}}">
                                                @endforeach
                                                <input class="form-control total" readonly type="text" name="total" />
                                            </td>
                                            <td width="10px">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="padding-top-10">
                                                @if($previousTreatmentList->count()>0 && $due>0)
                                                    <input onclick="openPayBox()" type="button" class="btn btn-info" value="Pay">
                                                    
                                                @endif
                                                <input type="submit" value="Submit" class="btn btn-success"> <br>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table> </form>
                                <hr>
                                @if($previousTreatmentList->count()>0 && $due>0)
                                <div class="col-md-12">
                                    <div class="col-md-4 col-md-offset-8">
                                        <div class="pay-box">
                                            <p class="text-danger pay-text-error">Sorry, enter valid amount</p>
                                            <input id="payFee" class="margin-top-10 margin-right-10 paid-field form-control" type="text">
                                            <input onclick="payAmount({{$reportDetails->id}})" class="margin-top-10 btn btn-info" value="OK" type="button">
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <input type="button" value="Add Row" onclick="addRow();" id="abtn"/>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="size">
            <input type="hidden" id="count" value="1">
        </section>
        <!-- /.content -->
        <section class="content">
            <div class="row">
                <!-- Form controls -->
                <div class="col-sm-12">
                    <div class="panel panel-bd">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>Payment Schedule</h4>
                            </div>
                        </div>
                        <div class="panel-body">

                            <div class="table-responsive ">
                                <form autocomplete="off" method="post" action="{{route('schedule.addNew',['reportId'=>$reportDetails->id])}}">
                                    {{csrf_field()}}
                                    <table class="table table-bordered table-hover" cellspacing="0" width="100%" id="tblPayment">
                                        <thead>
                                        <tr>
                                            <th> No</th>
                                            <th>Work</th>
                                            <th>Description</th>
                                            <th>Appointment</th>
                                            <th rowspan="{{$scheduleList->count()}}">Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i=1;
                                        @endphp
                                        @if($scheduleList->count()>0)

                                            @foreach($scheduleList as $schedule)
                                                <tr>
                                                    <th>{{$i++}}</th>
                                                    <th>{{$schedule->work}}</th>
                                                    <th>{{$schedule->description}}</th>
                                                    <th>{{$schedule->date}}</th>
                                                </tr>
                                            @endforeach
                                        @endif
                                        <tr>
                                            <td width="10px">{{$i++}}</td>
                                            <td width="150px">
                                                <input class="form-control margin-top-15" type="text" name="work[]" />
                                            </td>
                                            <td width="300px">
                                                <textarea class="form-control" name="description[]"></textarea>
                                            </td>
                                            <td width="120px">
                                                <input readonly class="form-control  margin-top-15 datepicker" type="text" name="appointment[]" />
                                            </td>

                                            <td width="10px">
                                                <input type="button" value="X" onclick="removeRoww(this);" />
                                            </td>

                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="col-md-3 col-md-offset-9 text-right">
                                        <input type="submit" value="Submit" class="btn btn-success">
                                    </div>
                                </form>
                                <hr>
                                <input type="button" value="Add Row" onclick="addRoww();" />
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
@endsection
@section('script')
    <script language="javascript">
        function setCost() {
            var  total = 0;
            $('select.form-control').each(function(){

                var unit = $(this).parent().parent().find('.unit').val();
                var unitValue =$('input[name='+this.value+']').val();
                var subTotal = unitValue*unit;
                total = Number(total)+Number(subTotal);
                $(this).parent().parent().find('.rate').val(unitValue);
                $(this).parent().parent().find('.cost').val(subTotal);
                $('.total').val(total);
            });
        }

        //add a new row to the table
        var rowNo =1;
        function addRow() {
            selected=[];
            for(i=0;i<document.getElementsByTagName("select").length;i++)
            {
                selected.push(document.getElementsByTagName("select")[i].value);
            }
            rowNo =Number(rowNo) + Number('1');
            //add a row to the rows collection and get a reference to the newly added row
            var newRow = document.all("tblGrid").insertRow();
            //add 3 cells (<td>) to the new row and set the innerHTML to contain text boxes
            var oCell = newRow.insertCell();
            count=parseInt(document.getElementById("count").value)+1;
            oCell.className="ro";
            oCell.innerHTML = count;

            var oCell = newRow.insertCell();
            x = 
                "                                                    @foreach($costList as $cost)                                                      val{{$cost->id}}++{{$cost->treatment_name}}@endforeach";
            ar=x.trim().split('val');
            document.getElementById("size").value=ar.length-1;
                  
            document.getElementById("count").value=count;
            if(count>=(ar.length-1))
            {
                document.getElementById("abtn").style.display = 'none';
            }
                  
            selecthtml="<select onchange='setCost()' class='form-control' name='treatment_name[]'> <option value='0'>Select one</option>";
            for(i=1;i<ar.length;i++)
            {       if(!selected.includes(ar[i].split("++")[0]))
            {
                selecthtml+="<option value='"+ar[i].split("++")[0]+"'>"+ar[i].split("++")[1]+"</option>";
            }
            }
                            
            selecthtml+="</select>";
            oCell.innerHTML=selecthtml;

            oCell = newRow.insertCell();
            oCell.innerHTML = "<input onkeyup='setCost()' value='0' class='form-control unit' type='text' name='unit[]'>";

            oCell = newRow.insertCell();
            oCell.innerHTML = "<input readonly value='0' class='form-control rate disabled' type='text' name='rate[]'> ";

            oCell = newRow.insertCell();
            oCell.innerHTML = "<input readonly value='0' class='form-control cost disabled' type='text' name='cost[]'> ";

            oCell = newRow.insertCell();
            oCell.innerHTML = "<input type='button' value='X' onclick='removeRow(this);'/> ";
        }

        //deletes the specified row from the table
        function removeRow(src) {

            /* src refers to the input button that was clicked.
             to get a reference to the containing <tr> element,
             get the parent of the parent (in this case case <tr>)
             */

            count=parseInt(document.getElementById("count").value)-1;
            document.getElementById("count").value=count;
            if(count<document.getElementById("size").value)
            {
                document.getElementById("abtn").style.display = 'block';
            }
            var oRow = src.parentElement.parentElement;

            //once the row reference is obtained, delete it passing in its rowIndex
            document.all("tblGrid").deleteRow(oRow.rowIndex);
            setCost();
            for(i=0;i<document.getElementsByClassName("ro").length;i++)
            {
                document.getElementsByClassName("ro")[i].innerHTML=i+1;
               
            }
        }
        /*-------------------------------------*/
        //add a new row to the table
        function addRoww() {
            //add a row to the rows collection and get a reference to the newly added row
            var newRow = document.all("tblPayment").insertRow();

            //add 3 cells (<td>) to the new row and set the innerHTML to contain text boxes
            var oCell = newRow.insertCell();
            oCell.innerHTML = "2";

            var oCell = newRow.insertCell();
            oCell.innerHTML = '<input class="form-control margin-top-15" type="text" name="work[]" />';

            oCell = newRow.insertCell();
            oCell.innerHTML = ' <textarea class="form-control" name="description[]"></textarea>';

            oCell = newRow.insertCell();
            oCell.innerHTML = '<input readonly class="form-control  margin-top-15 datepicker" type="text" name="appointment[]" />';

            oCell = newRow.insertCell();
            oCell.innerHTML = "<input type='button' value='X' onclick='removeRow1(this);'/>";


        }


        //deletes the specified row from the table
        function removeRow1(src) {
            /* src refers to the input button that was clicked.
             to get a reference to the containing <tr> element,
             get the parent of the parent (in this case case <tr>)
             */
            var oRow = src.parentElement.parentElement;

            //once the row reference is obtained, delete it passing in its rowIndex
            document.all("tblPayment").deleteRow(oRow.rowIndex);
        }

        /*open pay input field*/
        function openPayBox(){
            $(".pay-box").css("display", "block");
        }


        /*get time with am/ph formatted*/
        function getTime(date) {

            var hours = date.getHours();
            var minutes = date.getMinutes();
            var seconds =date.getSeconds();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            if(seconds<10){
                seconds = '0'+seconds;
            }
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            var strTime = hours + ':' + minutes + ':'+seconds+' ' + ampm;
            return strTime;
        }



        /*pay fees*/
        function payAmount(reportId){
            var amount = $("#payFee").val();
            var due = $('.due-amount').text();
            var remaining  = due-amount;
            var paid = '{{$paidList->sum('amount')}}';
            var date = new Date();
            var toatal_paid  = Number(paid)+Number(amount);
            var month = date.getMonth()+1;
            var tblRow = Number($('.paidList').children('tr').length)+1;
            if (month<10){
                month = '0'+month;
            }
            var payRow = '<tr>\n' +
                '<td>'+tblRow+'</td>\n' +
                '<td>'+date.getDate()+'-'+month+'-'+date.getFullYear()+'</td>\n' +
                '<td>'+getTime(date)+'</td>\n' +
                '<td>'+amount+'</td>\n' +
                '</tr>';
            var row = '<tr>\n' +
                '<td width="10px"></td>\n' +
                '<td width="200px"></td>\n' +
                '<td width="200px"></td>\n' +
                '<td width="190px">Paid</td>\n' +
                '<td class="padding-10" width="220px">'+amount+'</td>\n' +
                '<td width="10px"></td>\n' +
                '</tr>';
            if($.isNumeric( amount )&&$.isNumeric( due )){
                $.ajax({
                    url:'{{route('payment.paid')}}',
                    type : 'POST',
                    data:{'_token':'{{csrf_token()}}','reportId':reportId,'amount':amount,'due':due},
                    success: function(result){
                        if (!$.trim(result)) {
                            $('.paidList').append(payRow);
                            $('.submit-box').append(row);
                            $('.due-amount').text(remaining);
                            $('.paid-amount').text(toatal_paid);
                            $('.pay-text-error').css("display", "none");
                            $("#payFee").val('');
                            Notification(amount+' amount has been paid');
                            tblRow +=1;
                        }
                        else {
                            Notification('Amount has not been paid');
                            $('.pay-text-error').html(result);
                            $('.pay-text-error').css("display", "block");
                        }
                    },
                });
            }else{
                $('.pay-text-error').css("display", "block");
                Notification('You enter an invalid amount');
            }
        }
    </script>
    {{--Date-picker flatpickr js--}}
@endsection

