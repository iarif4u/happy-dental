<?php
/**
 * Created by PhpStorm.
 * User: Mr. Lab
 * Date: 12/1/2017
 * Time: 10:10 PM
 */
?>
@extends('layouts.blank')
@section('content')
    <!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->

        <!-- Main content -->
        <section class="content">
            <div class="row">


                        <div class="panel-body">
                            <div class="doc-info">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="doc-info">
                                            <h2>Dr.Debashis Dey(Rijon)</h2>
                                            <p>Somae Other Data</p>
                                            <p>Some Con Info</p>
                                            <p>Some Address</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="doc-info">
                                            <h2>Dr.Shilpi Biswas</h2>
                                            <p>Somae Other Data</p>
                                            <p>Some Con Info</p>
                                            <p>Some Address</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="doc-logo">
                                            <img src="images/" alt="Logo Here.." />
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="Patient-info">
                                        <div class="col-md-5">
                                            <p>Patient's Name : <span>{{$reportDetails->patient->name}}</span> </p>
                                        </div>
                                        <div class="col-md-2">
                                            <p>Age : <span>{{$reportDetails->patient->age}}</span> </p>
                                        </div>
                                        <div class="col-md-2">
                                            <p>Gender : <span>{{$reportDetails->patient->sex}}</span> </p>
                                        </div>
                                        <div class="col-md-3">
                                            <p>Date : <span>{{$prescriptionDetails->date}}</span> </p>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="cc">
                                            <p>{{$prescriptionDetails->c_c}}</p>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="oe">
                                            <p>{{$prescriptionDetails->o_e}}</p>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="oe">
                                            <p>{{$prescriptionDetails->advice}}</p>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">

                                    <div class="adv">

                                        <?php $count = 1; ?>
                                        @foreach($prescriptionDetails->PrescriptionMedicine as $medicine)
                                            <div class="allinfadv">
                                                <div class="col-md-1">
                                                    <p>{{$count++}}</p>
                                                </div>
                                                <div class="col-md-8">
                                                    <p><b>{{$medicine->medicine_name}}</b></p>
                                                </div>
                                                <div class="col-md-1">
                                                    <p><b>{{$medicine->morning}}</b></p>
                                                </div>
                                                <div class="col-md-1">
                                                    <p><b>{{$medicine->evening}}</b></p>
                                                </div>
                                                <div class="col-md-1">
                                                    <p><b>{{$medicine->night}}</b></p>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                            <hr/>

                        </div>





            </div>
        </section> <!-- /.content -->
    </div> <!-- /.content-wrapper -->
@endsection

