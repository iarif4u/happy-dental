<?php
/**
 * Created by PhpStorm.
 * User: Mr. Arif
 * Date: 12/2/2017
 * Time: 7:22 PM
 */
?>

@extends('layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">


            <div class="header-icon">
                <i class="pe-7s-note2"></i>
            </div>
            <div class="header-title">
                <h1>Treatment</h1>
                <small> Treatment List Infromation</small>
                <ol class="breadcrumb hidden-xs">
                    <li><a href="javascript:void(0);"><i class="pe-7s-home"></i> Home</a></li>
                    <li class="active">Treatment info</li>
                </ol>
            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="resultAjax"></div>
                    <div class="panel panel-bd lobidrag">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            <script>
                                // notification
                                setTimeout(function () {
                                    toastr.options = {
                                        closeButton: true,
                                        progressBar: true,
                                        showMethod: 'slideDown',
                                        timeOut: 1000
                                    };
                                    toastr.success('Notification', 'Treatment Add Success');

                                }, 2300);
                            </script>

                        @endif
                        @if (count($errors) > 0)
                            <div class="alert">
                                @foreach ($errors->all() as $error)
                                    <p class="each-error">{{ $error }} </p>
                                @endforeach
                            </div>
                        @endif
                        <div class="panel-heading">

                            <div class="btn-group">
                                <!-- Trigger the modal with a button -->
                                <button data-toggle="modal" data-target="#myModal" class="btn btn-success">
                                    <i class="fa fa-plus"></i> Add Treatment
                                </button>
                                <!-- Modal -->
                                <div id="myModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->

                                        <div class="modal-content">
                                            <div class="modal-header bg-green bg-success">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title color-white">Add Treatment</h4>
                                            </div>
                                            <div class="modal-body">

                                                {{ Form::text('dataName', null,['class'=>"form-control",'placeholder'=>'Enter Treatment Name','id'=>"dataName"])}}
                                                <br/>
                                                {{ Form::text('dataCost', null,['class'=>"form-control",'placeholder'=>'Enter Treatment Cost','id'=>"dataCost"])}}

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default closer" data-dismiss="modal">Close</button>

                                                <button onclick="addRowData()" type="button" class="btn btn-success">Save</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                {{--End modal--}}
                            </div>
                        </div>
                        <div class="panel-body">

                            <div class="panel-body">

                                <div id="example_wrapper" class="dataTables_wrapper">

                                    <table id="example" class="display nowrap dataTable dtr-inline" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 136px;">SL</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 136px;">Patient Name</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 136px;">Date</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 136px;">Time</th>
                                            <th class="dt-body-right sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 79px;">Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>

                                        <tr>
                                            <th rowspan="1" colspan="1">SL</th>
                                            <th rowspan="1" colspan="1">Patient Name</th>
                                            <th rowspan="1" colspan="1">Date</th>
                                            <th rowspan="1" colspan="1">Time</th>
                                            <th class="dt-body-right" rowspan="1" colspan="1">Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody id="tableBody">
                                        <?php
                                        $i =1;
                                        ?>
                                        @if($prescriptionList->count()>0)
                                            @foreach($prescriptionList as $prescription)
                                                <tr id="row{{$prescription->id}}" role="row" @if($i%2==0) class="odd" @else class="even" @endif>
                                                    <th class="count" rowspan="1" colspan="1">{{$i++}}</th>
                                                    <th class="insertDataName{{$prescription->id}}" rowspan="1" colspan="1">
                                                        {{$prescription->name}}
                                                    </th>
                                                    <th class="insertDataCost{{$prescription->id}}" rowspan="1" colspan="1">
                                                        {{$prescription->date}}
                                                    </th>
                                                    <th class="insertDataCost{{$prescription->id}}" rowspan="1" colspan="1">
                                                        {{$prescription->time}}
                                                    </th>
                                                    <th class="dt-body-right" rowspan="1" colspan="1">
                                                        <div  class="icon_box">
                                                            <a href="{{route('prescription.prescriptionDetails',['prescriptionId'=>$prescription->id])}}">
                                                                <i class="hvr-buzz-out fa fa-street-view"></i>
                                                            </a>
                                                        </div>



                                                    </th>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>


                                </div>

                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </section> <!-- /.content -->


    </div> <!-- /.content-wrapper -->


    </div> <!-- /.content-wrapper -->


@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            /*Add dataTabe of Patient list*/
            $('#example').DataTable();
        })
    </script>
@endsection