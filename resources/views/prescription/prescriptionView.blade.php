<?php
/**
 * Created by PhpStorm.
 * User: Mr. Lab
 * Date: 12/1/2017
 * Time: 8:33 PM
 */ ?>

@extends('layouts.master')
@section('content')
<script language="javascript">

    var tablSl = 1;
    var count=1;
    //add a new row to the table
    function addRow() {
        tablSl=+tablSl + +count;
        //add a row to the rows collection and get a reference to the newly added row
        var newRow = document.all("tblGrid").insertRow();

        //add 3 cells (<td>) to the new row and set the innerHTML to contain text boxes
        var oCell = newRow.insertCell();
        oCell.innerHTML = tablSl;

        var oCell = newRow.insertCell();
        oCell.innerHTML = "<input class='form-control medicine_data' type='text' name='medicine_name[]'>";

        var oCell = newRow.insertCell();
        oCell.innerHTML = '<select name="meal[]">\n' +
            '<option value="1">After</option>\n' +
            '<option value="0">Before</option>\n' +
            '</select>';

        oCell = newRow.insertCell();
        oCell.innerHTML = "<input class='form-control' type='text' name='morning[]'>";

        oCell = newRow.insertCell();
        oCell.innerHTML = "<input  class='form-control' type='text' name='evening[]'> ";

        oCell = newRow.insertCell();
        oCell.innerHTML = "<input  class='form-control' type='text' name='night[]'> ";

        oCell = newRow.insertCell();
        oCell.innerHTML = "<input type='button' value='X' onclick='removeRow(this);'/> ";


    }

    //deletes the specified row from the table
    function removeRow(src) {
        /* src refers to the input button that was clicked.
         to get a reference to the containing <tr> element,
         get the parent of the parent (in this case case <tr>)
         */
        var oRow = src.parentElement.parentElement;

        //once the row reference is obtained, delete it passing in its rowIndex
        document.all("tblGrid").deleteRow(oRow.rowIndex);
    }

</script>
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1> Add Prescription </h1>
            <small> Prescription Infromation</small>
            <ol class="breadcrumb hidden-xs">
                <li><a href="{{route('home')}}"><i class="pe-7s-home"></i> Home</a></li>
                <li class="active">Add Prescription</li>
            </ol>
        </div>
    </section>
    <!-- Main content -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Form controls -->
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="btn-group">
                            <a class="btn btn-primary" href="PrescriptionView.php"> <i class="fa fa-list"></i> Prescription View </a>
                        </div>
                    </div>

                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <form action="{{route('prescription.prescriptionMake',['reportId'=>$reportDetails->id])}}" class="col-sm-12" method="POST">
                            <div class="row">

                                {{csrf_field()}}
                                <div class="col-sm-6 form-group">
                                    <label>Patient Name: </label>
                                    {{$reportDetails->patient->name}}
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Age: </label>
                                    {{$reportDetails->patient->age}}
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Gender: </label>
                                    {{$reportDetails->patient->sex}}
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Date: </label>
                                    {{date('d-m-Y')}}
                                </div>
                                <div class="col-sm-12 form-group">
                                    <label>C/C</label>
                                    <textarea name="c/c" id="some-textarea" class="form-control" rows="3" placeholder="Enter C/C ..."></textarea>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <label>O/E</label>
                                    <textarea name="o/e" id="some-textarea" class="form-control" rows="3" placeholder="Enter O/E ..."></textarea>
                                </div>

                                <div class="col-sm-12 form-group">
                                    <label>Adv</label>
                                    <textarea name="advice" id="some-textarea" class="form-control" rows="3" placeholder="Enter Advice ..."></textarea>
                                </div>

                            </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" cellspacing="0" width="100%" id="tblGrid">
                                <thead>
                                <tr>
                                    <th> No</th>
                                    <th>Medicine Name</th>
                                    <th> Before Meal</th>
                                    <th>Morning</th>
                                    <th>Evening</th>
                                    <th>Night</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody id="tbl_body">
                                <tr>
                                    <td class="sl_no" width="10px">1</td>
                                    <td width="200px">
                                        <input class="form-control medicine_data" type="text" name="medicine_name[]" />
                                    </td>
                                    <td width="5px">
                                        <select name="meal[]">
                                            <option value="1">After</option>
                                            <option value="0">Before</option>
                                        </select>
                                        {{--<input class="form-control" type="checkbox" name="meal[]" />--}}
                                    </td>
                                    <td width="200px">
                                        <input class="form-control" type="text" name="morning[]" />
                                    </td>
                                    <td width="200px">
                                        <input class="form-control" type="text" name="evening[]" />
                                    </td>
                                    <td width="200px">
                                        <input class="form-control" type="text" name="night[]" /> </td>
                                    <td width="10px">
                                        <input type="button" value="X" onclick="removeRow(this);" />
                                    </td>
                                </tr>

                                </tbody>
                            </table>

                            <hr>
                            <input type="button" value="Add Row" onclick="addRow();" />
                            <div class="col-sm-12 reset-button">
                                <a href="#" class="btn btn-warning">Reset</a>
                                <input class="btn btn-success" type="submit" value="Save">

                            </div>
                        </div>


                    </form>
                    </div>

                </div>
            </div>
        </div>


    </section>
    <!-- /.content -->

</div>
<!-- /.content-wrapper -->
@endsection

@section('script')
    <script type="text/javascript">
        /*Script start for live search*/
        $(document).ready(function(){
            $(document).on('keyup', '.medicine_data', function(){
                var thisElement = $(this);
                var value = thisElement.val();
                if(value.length>=2) {
                    /*Send ajax request to get list data*/
                    $.ajax({
                        url: '{{route('prescription.liveMedicine')}}',
                        type: 'POST',
                        data: {'_token': '{{csrf_token()}}', 'value': value},
                        success: function (result) {
                            $('.search-result').hide();
                            thisElement.parent().append(result);
                        }
                    });
                }else{
                    $('.search-result').hide();
                }
            });
        });
        /*Script end for live-search*/

        /*Set a value to input field for live search*/
        $(document).on('click', '.result-list', function(){

           $(this).parent().parent().find('.medicine_data').val($(this).find('.medicineName').text());
            $(this).parent().hide();
        });

        /*Remove search-result when click out-side*/
        $(document).mouseup(function(e)
        {
            var container = $('.search-result');
            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
                container.hide();
            }
        });
    </script>


@endsection