<?php
/**
 * Created by PhpStorm.
 * User: Arif
 * Date: 10/30/2017
 * Time: 8:40 PM
 */
?>
@extends('layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                </div>
            </form>

            <div class="header-icon">
                <i class="pe-7s-note2"></i>
            </div>
            <div class="header-title">
                <h1> Add Details </h1>
                <small> Details InfromationInfromation</small>
                <ol class="breadcrumb hidden-xs">
                    <li><a href="{{route('home')}}"><i class="pe-7s-home"></i> Home</a></li>
                    <li class="active">Add Details</li>
                </ol>
            </div>
        </section>
        <!-- Main content -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- Form controls -->
                <div class="col-sm-12">
                    <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                            <div class="btn-group">
                                {{--<a class="btn btn-primary" href="DetailsView.php"> <i class="fa fa-list"></i>  Details View </a>--}}
                            </div>
                        </div>
                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                            <div class="row text-uppercase text-primary text-bold">
                                    <div class="col-md-2">Patient Name</div>
                                    <div class="col-md-3">{{$patientDetail->name}}</div>
                                    <div class="col-md-1">Age</div>
                                    <div class="col-md-2">{{$patientDetail->age}}</div>

                                    <div class="col-md-2">Moblile</div>
                                    <div class="col-md-2">{{$patientDetail->mobile}}</div>

                            </div>
                            <br>
                                {!! Form::open(['route' => 'report.reportPost']) !!}
                                {{csrf_field()}}
                                <input type="hidden" value="{{$patientDetail->id}}" name="patientId">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5>Chief Complain</h5>
                                    @if($chiefComplain->count()>0)
                                        @foreach($chiefComplain as $complain)
                                        <div class="col-md-4">
                                            <label class="radio-inline">
                                                {!! Form::checkbox('chief_complain[]', $complain->id) !!}
                                                {{$complain->complain_name}}
                                            </label>
                                        </div>
                                        @endforeach
                                    @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5>Location 1</h5>
                                        <div class="col-md-4">
                                            <div class="bdt">
                                                <label class="radio-inline">
                                                    {!! Form::checkbox('locationOne[]', "1") !!}1
                                                </label> <span></span> <label class="radio-inline">
                                                    {!! Form::checkbox('locationOne[]', "2") !!}2
                                                </label>
                                            </div>
                                            <div class="bdt2">
                                                <label class="radio-inline">
                                                    {!! Form::checkbox('locationOne[]', "3") !!}3

                                                </label> <span></span> <label class="radio-inline">
                                                    {!! Form::checkbox('locationOne[]', "4") !!}4
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5>On Examination/Findings</h5>
                                    @if($examination->count()>0)
                                        @foreach($examination as $exam)
                                        <div class="col-md-4">
                                            <label class="radio-inline">
                                                {!! Form::checkbox('examination[]', $exam->id) !!}
                                                {{$exam->examination_name}}
                                            </label>
                                        </div>
                                        @endforeach
                                    @endif
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col-md-12">
                                        <h5>Location 2</h5>
                                    <div class="col-md-12">
                                        <div class="bdt">
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '1') !!} 8
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '2') !!} 7
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '3') !!} 6
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '4') !!} 5
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '5') !!} 4
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '6') !!} 3
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '7') !!} 2

                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '8') !!} 1
                                            </label>
                                            <span></span>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '9') !!} 1
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '10') !!} 2
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '11') !!} 3
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '12') !!} 4
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '13') !!} 5
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '14') !!} 6
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '15') !!} 7
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '16') !!} 8
                                            </label>
                                        </div>

                                        <div class="bdt2">
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '17') !!} 8
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '18') !!} 7
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '19') !!} 6
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '20') !!} 5
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '21') !!} 4
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '22') !!} 3
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '23') !!} 2
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '24') !!} 1
                                            </label>
                                            <span></span>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '25') !!} 1
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '26') !!} 2
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '27') !!} 3
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '28') !!} 4
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '29') !!} 5
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '30') !!} 6
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '31') !!} 7
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationTwo[]', '32') !!} 8
                                            </label>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5>Medical History/Problem</h5>
                                    @if($medicalHistory->count()>0)
                                            @foreach($medicalHistory as $history)
                                                <div class="col-md-4">
                                                    <label class="radio-inline">
                                                        {!! Form::checkbox('medicalHistory[]', $history->id) !!}
                                                        {{$history->disease_name}}</label>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                    <h5>Advice</h5>
                                    @if($adviceList->count()>0)
                                        @foreach($adviceList as $advice)
                                            <div class="col-md-4">
                                                <label class="radio-inline">
                                                    {!! Form::checkbox('advice[]', $advice->id) !!}
                                                    {{$advice->advice_name}}
                                                </label>

                                            </div>
                                        @endforeach
                                    @endif
                                    </div>
                                </div>
                                <hr/>
                                <div class="col-sm-12 reset-button">
                                    <a href="#" class="btn btn-warning">Reset</a>
                                    {!! Form::submit('Save',['class'=>"btn btn-success"]) !!}
                                    {{--<input type="submit" class="btn btn-success" value="Save">--}}
                                </div>
                                {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </section> <!-- /.content -->
    </div> <!-- /.content-wrapper -->


@endsection