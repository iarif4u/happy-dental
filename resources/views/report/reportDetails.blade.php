<?php
/**
 * Created by PhpStorm.
 * User: Md. Arif
 * Date: 12/1/2017
 * Time: 12:07 AM
 */
//dd($reportDetails->ReportChiefComplain);
?>
@extends('layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                </div>
            </form>

            <div class="header-icon">
                <i class="pe-7s-note2"></i>
            </div>
            <div class="header-title">
                <h1> Add Details </h1>
                <small> Details InfromationInfromation</small>
                <ol class="breadcrumb hidden-xs">
                    <li><a href="{{route('home')}}"><i class="pe-7s-home"></i> Home</a></li>
                    <li class="active">Add Details</li>
                </ol>
            </div>
        </section>
        <!-- Main content -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- Form controls -->
                <div class="col-sm-12">
                    <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                            <div class="btn-group">
                            </div>
                        </div>
                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                                @if(Session::has('message'))
                                    <p class="alert alert-success">{{ Session::get('message') }}</p>
                                @endif
                            <div class="row text-uppercase text-primary text-bold">
                                <div class="col-md-2">Patient Name</div>
                                <div class="col-md-3">{{$reportDetails->patient->name}}</div>
                                <div class="col-md-1">Age</div>
                                <div class="col-md-2">{{$reportDetails->patient->age}}</div>
                                <div class="col-md-2">Moblile</div>
                                <div class="col-md-2">{{$reportDetails->patient->mobile}}</div>
                            </div>
                            <br>



                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Chief Complain</h5>
                                    @if($chiefComplain->count()>0)
                                        @foreach($chiefComplain as $complain)
                                            <div class="col-md-4">
                                                <label class="radio-inline">
                                                    <?php $chipComplainArray = array(); ?>
                                                    @foreach($reportDetails->ReportChiefComplain as $reportComplain)
                                                        <?php array_push($chipComplainArray,$reportComplain->complain_id); ?>
                                                    @endforeach
                                                    @if(in_array($complain->id,$chipComplainArray))
                                                        {!! Form::checkbox('chief_complain[]', $complain->id, true) !!}
                                                    @else
                                                        {!! Form::checkbox('chief_complain[]', $complain->id) !!}
                                                    @endif
                                                    {{$complain->complain_name}}
                                                </label>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Location 1</h5>
                                    <div class="col-md-4">
                                        <div class="bdt">
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationOne[]', "1") !!} 1
                                            </label> <span></span> <label class="radio-inline">
                                                {!! Form::checkbox('locationOne[]', "2") !!} 2
                                            </label>
                                        </div>
                                        <div class="bdt2">
                                            <label class="radio-inline">
                                                {!! Form::checkbox('locationOne[]', "3") !!} 3

                                            </label> <span></span> <label class="radio-inline">
                                                {!! Form::checkbox('locationOne[]', "4") !!} 4
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>On Examination/Findings</h5>
                                    <?php $examArray = array(); ?>
                                    @foreach($reportDetails->ReportExamination as $reportExam)
                                        <?php array_push($examArray,$reportExam->examination_id); ?>
                                    @endforeach
                                    @if($examination->count()>0)
                                        @foreach($examination as $exam)
                                            <div class="col-md-4">
                                                <label class="radio-inline">
                                                    @if(in_array($exam->id,$examArray))
                                                        {!! Form::checkbox('examination[]', $exam->id,true) !!}
                                                        @else
                                                        {!! Form::checkbox('examination[]', $exam->id) !!}
                                                    @endif

                                                    {{$exam->examination_name}}
                                                </label>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-12">
                                    <h5>Location 2</h5>
                                    <div class="col-md-12">
                                        <?php $locationArray = array(); ?>
                                        @foreach($reportDetails->ReportSecondayLocation as $location)
                                            <?php array_push($locationArray, $location->teeth_location_id); ?>
                                        @endforeach
                                        <div class="bdt">
                                            <?php $count = 9; ?>
                                            @for($i=1;$i<=16;$i++)
                                                    @if($i==9)
                                                        <span></span>
                                                    @endif
                                                @if($i==9)
                                                    <?php $right =1; ?>
                                                @endif
                                            <label class="radio-inline">
                                                @if(in_array($i,$locationArray))
                                                    {!! Form::checkbox('locationTwo[]', $i,true) !!}
                                                @else
                                                    {!! Form::checkbox('locationTwo[]', $i) !!}
                                                @endif
                                                @if(isset($right))
                                                    {{$right++}}
                                                    @else
                                                    {{$count-$i}}
                                                @endif

                                            </label>
                                            @endfor
                                        </div>
                                        <div class="bdt2">
                                            <?php $count = 9; ?>
                                            @for($i=1;$i<=16;$i++)
                                                <?php $position = $i+16; ?>

                                                <label class="radio-inline">
                                                    @if(in_array($position,$locationArray))
                                                        {!! Form::checkbox('locationTwo[]', $position,true) !!}
                                                    @else
                                                        {!! Form::checkbox('locationTwo[]', $position) !!}
                                                    @endif
                                                    @if($i>=9)
                                                        {{$i-8}}
                                                    @else
                                                        {{$count-$i}}
                                                    @endif
                                                </label>
                                                    @if($i==8)
                                                        <span></span>
                                                    @endif
                                            @endfor
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Medical History/Problem</h5>
                                    @if($medicalHistory->count()>0)
                                        <?php $historyArray = array(); ?>
                                        @foreach($reportDetails->ReportMedicalHistory as $reportHistory)
                                            <?php array_push($historyArray,$reportHistory->medical_history_id); ?>
                                        @endforeach
                                        @foreach($medicalHistory as $history)
                                            <div class="col-md-4">
                                                <label class="radio-inline">
                                                    @if(in_array($history->id,$historyArray))
                                                        {!! Form::checkbox('medicalHistory[]', $history->id,true) !!}
                                                    @else
                                                        {!! Form::checkbox('medicalHistory[]', $history->id) !!}
                                                    @endif
                                                    {{$history->disease_name}}</label>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Advice</h5>
                                    @if($adviceList->count()>0)
                                        <?php $adviceArray = array(); ?>
                                        @foreach($reportDetails->ReportAdvice as $reportAdvice)
                                            <?php array_push($adviceArray,$reportAdvice->advice_id); ?>
                                        @endforeach
                                        @foreach($adviceList as $advice)
                                            <div class="col-md-4">
                                                <label class="radio-inline">
                                                    @if(in_array($advice->id,$adviceArray))
                                                        {!! Form::checkbox('advice[]', $advice->id,true) !!}
                                                    @else
                                                        {!! Form::checkbox('advice[]', $advice->id) !!}
                                                    @endif
                                                    {{$advice->advice_name}}
                                                </label>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <hr/>
                            <div class="col-sm-12 reset-button">
                                <a class="btn btn-black" href="{{route('prescription.reportOldPrescription',['reportId'=>$reportId])}}">Prescription List</a>
                                <a class="btn btn-success" href="{{route('prescription.prescriptionView',['reportId'=>$reportId])}}">Make Prescription</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </section> <!-- /.content -->
    </div> <!-- /.content-wrapper -->


@endsection
