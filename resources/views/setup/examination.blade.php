<?php
/**
 * Created by PhpStorm.
 * User: Arif
 * Date: 10/30/2017
 * Time: 10:24 PM
 */
?>
@extends('layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>

            <div class="header-icon">
                <i class="pe-7s-note2"></i>
            </div>
            <div class="header-title">
                <h1>Examination</h1>
                <small> Examination List Infromation</small>
                <ol class="breadcrumb hidden-xs">
                    <li><a href="javascript:void(0);"><i class="pe-7s-home"></i> Home</a></li>
                    <li class="active">Examination info</li>
                </ol>
            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="resultAjax"></div>
                    <div class="panel panel-bd lobidrag">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            <script>
                                // notification
                                setTimeout(function () {
                                    toastr.options = {
                                        closeButton: true,
                                        progressBar: true,
                                        showMethod: 'slideDown',
                                        timeOut: 1000
                                    };
                                    toastr.success('Notification', 'Examination Add Success');

                                }, 2300);
                            </script>

                        @endif
                        @if (count($errors) > 0)
                                <div class="alert">
                                    @foreach ($errors->all() as $error)
                                        <p class="each-error">{{ $error }} </p>
                                    @endforeach
                                </div>
                            @endif
                            <div class="panel-heading">

                                <div class="btn-group">
                                    <!-- Trigger the modal with a button -->
                                    <button data-toggle="modal" data-target="#myModal" class="btn btn-success">
                                        <i class="fa fa-plus"></i> Add Examination
                                    </button>
                                    <!-- Modal -->
                                    <div id="myModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->

                                            <div class="modal-content">
                                                <div class="modal-header bg-green bg-success">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title color-white">Add Examination</h4>
                                                </div>
                                                <div class="modal-body">

                                                    {{ Form::text('examinationName', null,['class'=>"form-control",'placeholder'=>'Enter Examination Name','id'=>"examinationInsert"])}}

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default closer" data-dismiss="modal">Close</button>

                                                    <button onclick="addExamination()" type="button" class="btn btn-success">Save</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    {{--End modal--}}
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="panel-body">
                                    <div id="example_wrapper" class="dataTables_wrapper">

                                        <table id="example" class="display nowrap dataTable dtr-inline" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 136px;">SL</th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 136px;">Examination Name</th>
                                                <th class="dt-body-right sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 79px;">Action</th>
                                            </tr>
                                            </thead>
                                            <tfoot>

                                            <tr>
                                                <th rowspan="1" colspan="1">SL</th>
                                                <th rowspan="1" colspan="1">Examination Name</th>
                                                <th class="dt-body-right" rowspan="1" colspan="1">Action</th>
                                            </tr>
                                            </tfoot>
                                            <tbody id="tableBody">
                                            <?php
                                            $i =1;
                                            ?>
                                            @if($examinationList->count()>0)
                                                @foreach($examinationList as $examination)
                                                    <tr id="row{{$examination->id}}" role="row" @if($i%2==0) class="odd" @else class="even" @endif>
                                                        <th class="count" rowspan="1" colspan="1">{{$i++}}</th>
                                                        <th class="examinationName{{$examination->id}}" rowspan="1" colspan="1">{{$examination->examination_name}}</th>
                                                        <th class="dt-body-right" rowspan="1" colspan="1">
                                                            <div  class="icon_box">

                                                                <i onclick="editExamination({{$examination->id}})" class="hvr-buzz-out fa fa-edit"></i>
                                                            </div>
                                                            <div  class="icon_box">

                                                                <i onclick="deleteExamination({{$examination->id}})" class="hvr-buzz-out fa fa-trash has-error"></i>
                                                            </div>


                                                        </th>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>

                                        {{--Edit modal start--}}
                                        <button id="editModalBtn" data-toggle="modal" data-target="#editModal" class="hidden">

                                        </button>

                                        <div id="editModal" class="modal fade" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->

                                                <div class="modal-content">
                                                    <div class="modal-header bg-danger bg-green">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title color-white">Udate Examination Name</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <input type="text" id="examinationName" class="form-control">

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default closer" data-dismiss="modal">No</button>
                                                        <button type="button" class="btn btn-danger" onclick="editData()">Yes</button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{--End modal--}}



                                        {{--Edit modal End--}}


                                    <!-- Modal -->

                                        <button id="openDeleteModal" data-toggle="modal" data-target="#deleteModal" class="hidden">

                                        </button>

                                                                       <div id="deleteModal" class="modal fade" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->

                                                <div class="modal-content">
                                                    <div class="modal-header bg-danger bg-red">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title color-white">Warning</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <input type="hidden" id="examinationId">
                                                        <h3>Are you Sure???
                                                        </h3>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default closer" data-dismiss="modal">No</button>
                                                        <button type="button" class="btn btn-danger" onclick="deleteData()">Yes</button>

                                                    </div>
                                                </div>
                                                </form>

                                            </div>
                                        </div>
                                        {{--End modal--}}

                                    </div>

                                </div>

                            </div>
                    </div>

                </div>

            </div>
        </section> <!-- /.content -->


    </div> <!-- /.content-wrapper -->


    </div> <!-- /.content-wrapper -->


@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            /*Add dataTabe of Patient list*/
            $('#example').DataTable();
        })
    </script>
    <script type="text/javascript">
        /*Edit complain modal open*/
        function editExamination(examinationId){
            var name = $(".examinationName"+examinationId+"").text();
            $("#editModalBtn").click();
            $("#examinationId").val(examinationId);

            $("#examinationName").val(name);

        }

        /*Send ajax data to update complain name*/
        function editData(){
            /*Complain id get from hidden field*/
            var examinationId = $("#examinationId").val();

            /*Complain Name get from text field*/
            var examinationName = $("#examinationName").val();

            /*Send ajax request to update data*/
            $.ajax({
                url:'{{route('setup.CreateUpdateExamination')}}',
                type : 'POST',
                data:{'_token':'{{csrf_token()}}','examinationId':examinationId,'examinationName':examinationName},
                success: function(result){
                    if (!$.trim(result)) {
                        var rowData = ' <div class="alert alert-success">\n' +
                            'Examination Name update' +
                            '                            </div>';

                        $('.resultAjax').html(rowData);

                        $(".examinationName"+examinationId+"").fadeOut(function() {
                            $(this).text(examinationName)
                        }).fadeIn();

                        setTimeout(function () {
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                showMethod: 'slideDown',
                                timeOut: 1000
                            };
                            toastr.success('Notification', 'Examination Update Success');
                        }, 200);
                    }
                    else {
                        $('.resultAjax').html(result);
                    }
                }});
            $('.closer').click();

        }

        /*Delete complain modal open*/
        function deleteExamination(examinationId){
            $("#openDeleteModal").click();
            $("#examinationId").val(examinationId);

        }

        /*Send ajax post request to delete complain*/
        function deleteData(){
            var examinationId =$("#examinationId").val();
            var rowData = ' <div class="alert alert-success">\n' +
                'Examination Delete Success' +
                '                            </div>';
            $.ajax({
                url:'{{route('setup.deleteExamination')}}',
                type : 'POST',
                data:{'_token':'{{csrf_token()}}','examinationId':examinationId},
                success: function(result){
                    $("#row"+examinationId+"").hide('slow');
                    $('.resultAjax').html(rowData);
                }});
            $('.closer').click();
            // notification
            setTimeout(function () {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 1000
                };
                toastr.success('Notification', 'Examination Delete Success');

            }, 2300);
        }

        /*add examination by ajax*/
        function addExamination(){

            var examinationId = 0;
            var examinationName = $("#examinationInsert").val();
            /*Send ajax request to update data*/
            $.ajax({
                url:'{{route('setup.CreateUpdateExamination')}}',
                type : 'POST',
                data:{'_token':'{{csrf_token()}}','examinationId':examinationId,'examinationName':examinationName},
                success: function(result){
                    if (!$.trim(result)) {

                        $.ajax({

                            url:"{{route('setup.examId')}}",
                            type : 'POST',
                            data:{'_token':'{{csrf_token()}}'},
                            success: function(result){
                                $(".dataTables_empty").fadeOut();
                                clearInputField();
                                /*var rowCount = $('#example tr .count').length;

                                var sl = parseInt(rowCount)+1; */                               /*Row Start*/
                            var entryRow = ' <tr id="row'+result+'" role="row">\n' +
                                '                                                    <th class="count" rowspan="1" colspan="1">'+1+'</th>\n' +
                                '                                                    <th class="examinationName'+result+'" rowspan="1" colspan="1">'+examinationName+'</th>\n' +
                                '                                                    <th class="dt-body-right" rowspan="1" colspan="1">\n' +
                                '                                                        <div  class="icon_box">\n' +
                                '\n' +
                                '                                                            <i onclick="editExamination('+result+')" class="hvr-buzz-out fa fa-edit"></i>\n' +
                                '                                                        </div>\n' +
                                '                                                        <div  class="icon_box">\n' +
                                '\n' +
                                '                                                            <i onclick="deleteExamination('+result+')" class="hvr-buzz-out fa fa-trash has-error"></i>\n' +
                                '                                                        </div>\n' +
                                '\n' +
                                '\n' +
                                '                                                    </th>\n' +
                                '                                                </tr>';

                                /*Row End*/

                                $("#tableBody").append(entryRow);
                                $("#examinationInsert").val('');

                                var rowData = ' <div class="alert alert-success">\n' +
                                    'Examination Name Added Successfully Done' +
                                    '                            </div>';

                                $('.resultAjax').html(rowData);

                                var cn =1;
                                $(".count").each(function(){
                                    $(this).text(cn++)
                                });

                               Notification('Examination Added Success');
                            }
                        });

                        // $('.resultAjax').html(examId);
                    }
                    else {
                        $('.resultAjax').html(result);
                    }
                }
            });
            $('.closer').click();


        }
    </script>
@endsection