<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

/*Route group access only  login user*/
Route::group(['middleware'=>'auth'],function() {
#redirect after successfully login
    Route::get('/home', function () {
        return view('welcome');
    })->name('home');

    /*Patient Route Start*/
    Route::group(['prefix' => 'patient', 'as' => 'patient.'], function () {
        /*Patient add*/
        Route::get('add', ['uses' => 'PatientController@GetAddPatient'])->name('patientAdd');
        Route::post('add', ['uses' => 'PatientController@PostAddPatient']);
        Route::post('edit', ['uses' => 'PatientController@UpdatePatient'])->name('patientEdit');
        Route::post('delete', ['uses' => 'PatientController@DeletePatient'])->name('patientDelete');

        Route::get('list', ['uses' => 'PatientController@GetPatientList'])->name('getPatientList');
    });
    /*Patient Route end*/
    /*Schedule Route Start*/
    Route::group(['prefix' => 'schedule', 'as' => 'schedule.'], function () {
        /*Payment add to report*/
        Route::post('new/{reportId}', ['uses' => 'ScheduleController@addNewSchedule'])->name('addNew');

    });
    /*Schedule Route end*/

    /*Import-Export Route Start*/
    Route::group(['prefix' => 'import-export', 'as' => 'importExport.'], function () {
        /*Payment add to report*/
        Route::get('/', ['uses' => 'ExportImportController@getImportExportView'])->name('getView');
        Route::post('/import-file', ['uses' => 'ExportImportController@uploadExcelFile'])->name('uploadExcel');
        Route::get('/export-file/{type}', ['uses' => 'ExportImportController@downloadExcel'])->name('downloadExcel');


    });
    /*Imp-Export Route end*/

    /*Payment Route Start*/
    Route::group(['prefix' => 'payment', 'as' => 'payment.'], function () {
        /*Payment add to report*/
        Route::get('report/{reportId}', ['uses' => 'PaymentController@GetReportPayment'])->name('report');

        Route::post('calculate/{reportId}', ['uses' => 'PaymentController@PostReportPayment'])->name('postPaymentAdd');
        Route::post('paid-clint', ['uses' => 'PaymentController@PaidClintFee'])->name('paid');

    });
    /*Payment Route end*/


    /*Report Route Start*/
    Route::group(['prefix' => 'report', 'as' => 'report.'], function () {
        /*Report add*/
        Route::get('patient-to-report/{patientId}', ['uses' => 'ReportController@GetReportView'])->name('toPatient');
        Route::get('add-report', ['uses' => 'ReportController@GetReportView'])->name('reportAdd');
        Route::post('post-report', ['uses' => 'ReportController@PostReportAdd'])->name('reportPost');
        Route::get('report-view/{reportId}', ['uses' => 'ReportController@GetReportDetails'])->name('reportDetails');
        Route::get('list', ['uses' => 'ReportController@GetReportList'])->name('list');
    });
    /*Report Route end*/

    /*Prescription Route Start*/
    Route::group(['prefix' => 'prescription', 'as' => 'prescription.'], function () {
        /*Prescription add view*/
        Route::get('report-to-prescription/{reportId}', ['uses' => 'PrescriptionController@GetPrescriptionView'])->name('prescriptionView');

        Route::get('prescription-list-report/{reportId}', ['uses' => 'PrescriptionController@GetPrescriptionListByReport'])->name('reportOldPrescription');

        /*Prescription add */
        Route::post('make-prescription/{reportId}','PrescriptionController@PostMakePrescription')->name('prescriptionMake');

        /*Prescription View*/
        Route::get('view/{prescriptionId}', ['uses' => 'PrescriptionController@GetPrescriptionDetails'])->name('prescriptionDetails');

        /*Prescription List*/
        Route::get('list-view', ['uses' => 'PrescriptionController@GetPrescriptionList'])->name('prescriptionList');

        /*Prescription Download*/
        Route::get('prescription-pdf/{prescriptionId}', ['uses' => 'PrescriptionController@DownloadPrescription'])->name('prescriptionDownload');

        /*Medicine List search */
        Route::post('medicine-search/','PrescriptionController@SearchMedicine')->name('liveMedicine');


    });
    /*Prescription Route end*/

    /*Setup Route Start*/
    Route::group(['prefix' => 'setup', 'as' => 'setup.'], function () {

        /*Get insert Data id*/
        Route::post('data-id', 'SetupController@GetSessionData')->name('dataId');

        /*Chief Complain Start*/
        Route::get('chief-complain', ['uses' => 'SetupController@GetChiefComplain'])->name('chiefComplain');
        Route::post('chief-complain', 'SetupController@PostChiefComplain')->name('postChiefComplain');
        Route::post('delete-chief-complain', 'SetupController@DeleteChiefComplain')->name('deleteComplian');
        Route::post('update-chief-complain', 'SetupController@UpdateChiefComplain')->name('updateComplian');

        /*Chief Complain End*/

        /*Examination Start*/
        Route::get('examination', 'SetupController@GetExamination')->name('getExamination');
        Route::post('examination-update', 'SetupController@CreateUpdateExamination')->name('CreateUpdateExamination');
        Route::post('examination-delete', 'SetupController@DeleteExamination')->name('deleteExamination');
        Route::post('examination-id', 'SetupController@GetSessionData')->name('examId');
        /*Examination End*/

        /*Medical history  Start*/
        Route::get('medical-history', 'SetupController@GetMedicalHistory')->name('history');
        Route::post('medical-history/insert', 'SetupController@AddMedicalHistory')->name('medicalHistoryAdd');
        Route::post('medical-history/update', 'SetupController@AddMedicalHistory')->name('MedicalHistoryEdit');
        Route::post('medical-history/delete', 'SetupController@DeleteMedicalHistory')->name('deleteMedicalHistory');

        /*Medical history End*/

        /*Advice Start*/
        Route::get('medical-advice', 'SetupController@GetAdvice')->name('advice');
        Route::post('medical-advice/insert', 'SetupController@AddUpdateAdvice')->name('AdviceAdd');
        Route::post('medical-advice/update', 'SetupController@AddUpdateAdvice')->name('AdviceDataEdit');
        Route::post('medical-advice/delete-data', 'SetupController@DeleteAdviceData')->name('deleteAdviceData');
        /*Advice End*/

        /*Treatment Cost Start*/
        Route::get('treatment-cost', 'SetupController@GetTreatmentCost')->name('treatmentCost');
        Route::post('treatment-cost/update', 'SetupController@AddEditTreatmentCost')->name('treatmentDataEdit');
        Route::post('treatment/delete', 'SetupController@DeleteTreatmentCost')->name('deletetreatmentData');
        Route::post('treatment/insert', 'SetupController@AddEditTreatmentCost')->name('treatmentAdd');
        /*Treatment Cost End*/

    });
    /*setup Route end*/




});
